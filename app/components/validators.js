import isEmail from 'validator/lib/isEmail';

export const defaultMinLen = 2;
export const defaultMaxLen = 50;
export const required = (fieldName, msg = `${fieldName} cannot be left blank`) => (value) => value ? undefined : msg;
export const minLength = (fieldName, len) => (value) => {
  if (!value || fieldName === 'Amount' || fieldName === 'Other EMIs (optional)' || fieldName === 'Mobile Number'
  || fieldName === 'Monthly Net Take Home Salary') {
    return undefined;
  }

  if (fieldName === 'Confirm Password') {
    return value.length < len ? `Please enter at least ${len} characters` : undefined;
  }

  if (fieldName === 'Aadhaar Number') {
    return value.replace(/ /g, '').length < 12 ? `${fieldName} must have at least 12 characters` : undefined;
  }

  return value.length < len ? `${fieldName} must have at least ${len} characters` : undefined;
};
export const maxLength = (fieldName, len) => (value) => {
  if (!value) {
    return undefined;
  }

  if (fieldName === 'Aadhaar Number') {
    return value.replace(/ /g, '').length > 12 ? `${fieldName} can not have more than 12 characters` : undefined;
  }

  return value.length > len ? `${fieldName} can not have more than ${len} characters` : undefined;
};

export const minValue = (fieldName, len) => (value) => {
  if (!value) {
    return undefined;
  }
  return value < len ? `${fieldName} must have at least ${len}` : undefined;
};
export const maxValue = (fieldName, len) => (value) => {
  if (!value) {
    return undefined;
  }
  return value > len ? `${fieldName} can not have more than ${len}` : undefined;
};

export const isValidEmail = (fieldName) => (value) => {
  if (!value) {
    return undefined;
  }
  return isEmail(value) ? undefined : `Please enter a valid ${fieldName}`;
};
export const alwaysValid = () => undefined;

export const isDate = (fieldName) => (value) => {
  if (value) {
    return value.match(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/)
    ? undefined
    : `${fieldName === 'Date Of Birth (DD/MM/YYYY)' ? 'Date Of Birth' : fieldName} must be a valid date`;
  }
  return undefined;
};
