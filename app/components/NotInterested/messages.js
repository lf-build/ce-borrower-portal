/*
 * NotInterested Messages
 *
 * This contains all the text for the NotInterested component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.NotInterested.header',
    defaultMessage: 'This is the NotInterested component !',
  },
});
