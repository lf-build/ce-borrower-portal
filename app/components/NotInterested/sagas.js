import { put, takeLatest } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { navigationHelper } from '../../containers/NavigationHelper/helper';

// import { LOCATION_CHANGE } from 'react-router-redux';
import {
  NOT_INTERESTED_REQUEST,
} from './constants';
import * as actions from './actions';

export function* notInterestedOffer(action) {
  const form = 'review';
  // Select username from store
  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'not-interested',
  }, { tag: form, payload: { reason: action.meta.reason } });

  if (executionRequestAccepted) {
    yield put(actions.notInterestedStarted());

    // Wait for workflow to either fail or succeed.
    const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);
    if (valid) {
      navigationHelper.goTo({ name: 'initialOffer' }, 'notInterestedPage', false);
      yield put(actions.notInterestedFulfilled(valid.meta));
    } else {
      yield put(actions.notInterestedFailed(invalid.meta));
      navigationHelper.goTo({ name: 'initialOffer' }, 'internalServerError', false);
    }
  } else {
    yield put(actions.notInterestedDenied({ message: 'Workflow did not accept the request.' }));
    navigationHelper.goTo({ name: 'initialOffer' }, 'internalServerError', false);
  }

  yield put(actions.notInterestedEnded());
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeLatest(NOT_INTERESTED_REQUEST, notInterestedOffer);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
