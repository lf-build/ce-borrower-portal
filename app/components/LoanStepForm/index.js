/**
*
* LoanStepForm
*
*/
import { reduxForm } from 'redux-form/immutable';
import { connect } from 'react-redux';
import React from 'react';
// import styled from 'styled-components';
import LoanStep from '../../containers/LoanStep';


export class LoanStepForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const onSubmit = (values) => values;
    const { name, ...rest } = this.props;

    const Form = reduxForm({
      form: name,
      onSubmit,
      destroyOnUnmount: false,
    })(LoanStep);

    return <Form formAttached name={name} {...rest} />;
  }
}

LoanStepForm.propTypes = {
  name: React.PropTypes.string.isRequired,
  dispatch: React.PropTypes.func.isRequired,
};

export default connect()(LoanStepForm);
