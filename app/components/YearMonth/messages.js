/*
 * YearMonth Messages
 *
 * This contains all the text for the YearMonth component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.YearMonth.header',
    defaultMessage: 'This is the YearMonth component !',
  },
});
