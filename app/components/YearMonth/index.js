/**
*
* YearMonth
*
*/

import React from 'react';
import { Field } from 'redux-form/immutable';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import MenuItem from 'material-ui/MenuItem';

class YearMonth extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      year: undefined,
      showDiv: true,
    };
  }

  componentWillMount() {
    // here in componentWillMount, we will check if the device is a touch device or not
    // so that it will help in handling the touch/click on Aadhaar and Pan Number
    try {
      document.createEvent('TouchEvent');
    } catch (e) {
      this.setState({ showDiv: false });
    }
  }

  isYearRequired = () => {
    const { addYear } = this.props;
    if (addYear === undefined) {
      return '';
    }
    return undefined;  // enable next btn
  }


  render() {
    const year = ['1', '2', '3', '4', '5', '5+'];

    const onTransparentDivTouch = (e) => {
      if (e.type && e.type === 'touchend') {
        this.setState({ showDiv: false });
      }
    };
    return (
      <div style={{ width: '100%' }}>
        <span>
          <Field
            name={this.props.name}
            component={SelectField}
            hintText="Year"
            value={this.state.year}
            // onChange={handleYearChange}
            style={{ width: '35%', marginLeft: '0%', fontSize: '14px' }}
            hintStyle={{ fontSize: '14px' }}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            maxHeight={200}
            iconStyle={{ height: '24px', width: '24px', paddingRight: '20px' }}
            labelStyle={{ paddingRight: 'none' }}
            validate={this.isYearRequired}
          >
            {year.map((yr) => <MenuItem key={yr} value={yr} primaryText={`0${yr}`} />)}
          </Field>
        </span>
        <div className={this.state.showDiv ? 'transparent-div' : 'transparent-div hidden'} onTouchTap={(e) => onTransparentDivTouch(e)}></div>
      </div >
    );
  }
}

YearMonth.propTypes = {
  addYear: React.PropTypes.string,
  name: React.PropTypes.string,
};

export default YearMonth;
