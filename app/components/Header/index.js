/**
*
* Header
*
*/

import React from 'react';
import AppBar from 'material-ui/AppBar';
import styled from 'styled-components';
import FlatButton from 'material-ui/FlatButton';
import { browserHistory } from 'react-router';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import IdentitySessionPanel from '../IdentitySessionPanel';
import tenantLogo from './assets/images/logo.png';
import tenantLogoRBL from './assets/images/logo2.png';
import BuildInfo from '../BuildInfo';
import { userSignedOut } from '../../sagas/auth/actions';

const Wrapper = styled.div`
  position: fixed;
  width: 100%;
  z-index: 99;
`;

class Header extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    function handleTouchTap() {}
    const styles = {
      title: {
        cursor: 'pointer',
        height: '58px',
        marginBottom: '6px',
      },
      appBar: {
        backgroundColor: 'white',
        color: 'black',
      },
    };

    const { hideIdentitySessionPanel, signOut } = this.props;

    const pathSplit = location.pathname.split('/');
    const page = pathSplit.length >= 3 ? pathSplit[2] : 'none';

    // commenting code as per CEPROD-376 - no need to show Qbeara + RBL logo
    // const pagesForRBLLogo = [
    //   'banking',
    //   'education',
    //   'employment',
    //   'social',
    //   'final-offer',
    //   'application-loan-agreement',
    //   'application-under-review',
    // ];
    // const showRBLLogo = pagesForRBLLogo.filter((p) => p === page).length > 0;
    const showRBLLogo = false;

    const noHomePage = [
      'request-failed',
      'welcome',
      'application-expired',
      'verification-failed',
      'netlink',
      'cibil-failed',
    ];

    const clearCache = noHomePage.filter((p) => p === page).length > 0;

    const handleHomeButtonClick = () => {
      if (clearCache) {
        signOut();
      }

      if (!clearCache) {
        browserHistory.replace('/application/amount');
      }
    };

    return (
      <Wrapper>
        <AppBar
          style={styles.appBar}
          title={
            <span className={'logo'}>
              <a href="https://www.qbera.com" target="_blank">
                <img alt="Qbera" src={showRBLLogo ? tenantLogoRBL : tenantLogo} style={styles.title} />
              </a>
              <BuildInfo />
            </span>}
          onTitleTouchTap={handleTouchTap}
          showMenuIconButton={false}
          iconElementRight={hideIdentitySessionPanel || page === 'verification-failed' ?
            <div className={`welcomeMsg ${page === 'welcome' ? 'hidden' : ''}`}>
              <FlatButton onTouchTap={() => handleHomeButtonClick()} label={'Home'} />
            </div> : <IdentitySessionPanel />}
        />
      </Wrapper>
    );
  }
}

Header.propTypes = {
  hideIdentitySessionPanel: React.PropTypes.bool,
  signOut: React.PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({

});

function mapDispatchToProps(dispatch) {
  return {
    signOut: () => dispatch(userSignedOut()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Header);
