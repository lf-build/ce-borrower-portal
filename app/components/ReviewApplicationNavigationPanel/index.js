/**
*
* ReviewApplicationNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import Checkbox from 'material-ui/Checkbox';
import RaisedButton from 'material-ui/RaisedButton';
// import { browserHistory } from 'react-router';
import styled from 'styled-components';

import HintText from '../../components/HintText';
import { InlineEditIcon } from '../../components/Icons';

const ErrorDiv = styled.div`
line-height: 20px;
color: rgb(255, 117, 106);
transition: all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms;
text-align: left;
margin-top: 6px;
`;

// const Heading = styled.p`
//   font-size: 12px;
//   line-height: 20px;
//   letter-spacing: 0.045em;
//   color: #565656;
// `;

export class ReviewApplicationNavigationPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = { agree: false };
  }
  handleIAgree = (e, newAgree) => {
    this.setState({ agree: newAgree });
  }

  validationErrorHtml = (show, errors) => {
    if (show && errors) {
      // const errors = this.performValidation();
      // console.log('showing ', show, errors);

      const { basicInformation, details } = errors;

      const errorsIn = () => {
        if (basicInformation && details) {
          return 'Name and Addresses';
        } else if (basicInformation) {
          return 'Name';
        } else if (details) {
          return 'Addresses';
        }

        return '';
      };

      return (
        <div style={{ fontSize: '12px' }}>
          <ErrorDiv style={{ display: `${basicInformation || details ? 'block' : 'none'}` }}>
            It seems like there are some error(s) in provided {errorsIn()}. Click on the pencil icon <InlineEditIcon style={{ cursor: 'default' }} /> to check and correct.
          </ErrorDiv>
          {/* <div id="basicInfoErrors" style={{ display: `${basicInformation ? 'block' : 'none'}` }} >
            <ErrorDiv>Basic Information <InlineEditIcon onClick={() => browserHistory.replace('/application/basic-information')} /></ErrorDiv>
            <ErrorDiv style={{ display: `${basicInformation.middleName ? 'block' : 'none'}` }}>
              {basicInformation.middleName}
            </ErrorDiv>
            <ErrorDiv style={{ display: `${basicInformation.lastName ? 'block' : 'none'}` }}>
              {basicInformation.lastName}
            </ErrorDiv>
          </div> */}

          {/* <br /> */}
          {/* <div id="addressInfoErrors" style={{ display: `${details ? 'block' : 'none'}` }} >
            <ErrorDiv>Personal Details <InlineEditIcon onClick={() => browserHistory.replace('/application/personal-details')} /></ErrorDiv>
            <div style={{ display: `${details.currentResidentialAddress ? 'block' : 'none'}` }}>
              <Heading>Current Residential Details</Heading>
              <ErrorDiv style={{ display: `${details.currentResidentialAddress.addressLine1 ? 'block' : 'none'}` }}>
                {details.currentResidentialAddress.addressLine1}
              </ErrorDiv>
              <ErrorDiv style={{ display: `${details.currentResidentialAddress.addressLine2 ? 'block' : 'none'}` }}>
                {details.currentResidentialAddress.addressLine2}
              </ErrorDiv>
              <ErrorDiv style={{ display: `${details.currentResidentialAddress.locality ? 'block' : 'none'}` }}>
                {details.currentResidentialAddress.locality}
              </ErrorDiv>
            </div>

            <br />

            <div style={{ display: `${details.permanentResidentialAddress ? 'block' : 'none'}` }}>
              <Heading>Permanent Residential Details</Heading>
              <ErrorDiv style={{ display: `${details.permanentResidentialAddress && details.permanentResidentialAddress.addressLine1 ? 'block' : 'none'}` }}>
                {details.permanentResidentialAddress && details.permanentResidentialAddress.addressLine1}
              </ErrorDiv>
              <ErrorDiv style={{ display: `${details.permanentResidentialAddress && details.permanentResidentialAddress.addressLine2 ? 'block' : 'none'}` }}>
                {details.permanentResidentialAddress && details.permanentResidentialAddress.addressLine2}
              </ErrorDiv>
              <ErrorDiv style={{ display: `${details.permanentResidentialAddress && details.permanentResidentialAddress.locality ? 'block' : 'none'}` }}>
                {details.permanentResidentialAddress && details.permanentResidentialAddress.locality}
              </ErrorDiv>
            </div>
          </div> */}
        </div>
      );
    }

    return <div />;
  };

  render() {
    const { isLoadingOrSubmiting, submitApplication, errors, show } = this.props;
    const { agree } = this.state;
    const handleIAgree = this.handleIAgree;
    const offsetValue = window.innerWidth && window.innerWidth <= 540 ? 2 : 4;
    return (
      <div>
        <div className="row">
          <div className="col-xs-12">
            <Checkbox
              disabled={isLoadingOrSubmiting}
              onCheck={handleIAgree} checked={agree || isLoadingOrSubmiting}
              name="agree"
              label="I understand and agree to following terms:"
              labelStyle={{ fontSize: '15px', letterSpacing: '0.025em' }}
            />
            <ul style={{ marginTop: '10px', marginLeft: '55px', fontSize: '11px', lineHeight: '18px' }}>
              {/* <li>I allow Qbera to verify my device location from my mobile operator.</li> */}
              {/* <li>I authorize Qbera and / or its affiliates to contact me. This will override registry on DND / NDNC.</li>*/}
              <li>I understand that Qbera and / or its affiliates will be pulling my credit report from the credit bureaus to process my loan.</li>
              <li>I understand that Qbera and / or its affiliates will be pulling my KYC documents from DigiLocker to process my loan.</li>
            </ul>
            {/* <br />
            <HintText message={'You may receive a text on your registered mobile number to allow Qbera to know your location. Please follow the instructions given in the text.'} /> */}
          </div>
        </div>
        {this.validationErrorHtml(show, errors)}
        <div className="row">
          <div className={`col-xs-5 col-xs-offset-${offsetValue}`}>
            <RaisedButton
              className={'review-screen-button'}
              disabled={isLoadingOrSubmiting || !agree}
              onTouchTap={submitApplication}
              label="Get My Loan Offer"
              labelColor="#fff"
              labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
              backgroundColor="#3663ad"
            />
          </div>
        </div>
      </div>);
  }
}

ReviewApplicationNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  // handleGoBack: React.PropTypes.func.isRequired,
  // handleGoNext: React.PropTypes.func.isRequired,
  submitApplication: React.PropTypes.func.isRequired,
  errors: React.PropTypes.object,
  show: React.PropTypes.bool,
};

export default ReviewApplicationNavigationPanel;
