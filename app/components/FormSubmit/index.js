/**
*
* FormSubmit
*
*/
import React from 'react';
import { connect } from 'react-redux';
import { submit } from 'redux-form';

export default function withFormSubmit(WrappedComponent) {
  return connect()(class extends React.Component { // eslint-disable-line react/prefer-stateless-function
    static propTypes = {
      dispatch: React.PropTypes.func,
    }

    render() {
      const { dispatch } = this.props;
      const submitForm = (form) => dispatch(submit(form));
      return <WrappedComponent submitForm={submitForm} {...this.props} />;
    }
  });
}
