/**
*
* InternalServerError
*
*/

import React from 'react';
import styled from 'styled-components';

import { FormattedMessage } from 'react-intl';
import messages from './messages';
import LoanStep from '../../containers/LoanStep';
import { DangerIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 600;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

class InternalServerError extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep
        title={messages.header}
        reduceWidth
        navigationPanel={() => <span />}
      >
        <DangerIcon />
        <CongratulationText />
        <CongratulationText>
          <FormattedMessage {...messages.body} />
        </CongratulationText>
      </LoanStep>
    );
  }
}

InternalServerError.propTypes = {

};

export default InternalServerError;
