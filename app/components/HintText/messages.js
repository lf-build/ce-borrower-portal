/*
 * HintText Messages
 *
 * This contains all the text for the HintText component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.HintText.header',
    defaultMessage: 'This is the HintText component !',
  },
});
