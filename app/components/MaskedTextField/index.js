import React from 'react';
import TextField from 'redux-form-material-ui/lib/TextField';
import { conformToMask } from 'react-text-mask';
// import MaskedInput from 'react-text-mask';
// import { placeholderChar as defaultPlaceholderChar } from './masks';

class MaskedInput extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { value, mask, focus, ...reset } = this.props;
    const config = {
      guide: false,
    };
    const maskToApply = typeof mask === 'function' ? mask(value.toString()) : mask;
    const normalizedMask = maskToApply.filter((m) => typeof m === 'object');
    const result = conformToMask(value.toString(), maskToApply, config);
    const renderMask = result.meta.someCharsRejected || normalizedMask.length < value.length ? value : result.conformedValue;
    // console.log(maskToApply, value, result.meta.someCharsRejected, normalizedMask.length, value.length, normalizedMask.length < value.length);
    return <input value={focus ? value : renderMask} {...reset} />;
  }
}

MaskedInput.propTypes = {
  mask: React.PropTypes.any,
  focus: React.PropTypes.bool,
  value: React.PropTypes.any,
};


class MaskedTextField extends React.Component { // eslint-disable-line
  constructor(props) {
    super(props);
    this.state = {
      focus: false,
    };
  }
  render() {
    const onFocus = (...args) => {
      this.setState({
        focus: true,
      });
      if (this.props.input.onFocus) {
        this.props.input.onFocus(...args);
      }
    };
    const onFocusLost = (...args) => {
      this.setState({
        focus: false,
      });
      if (this.props.input.onBlur) {
        this.props.input.onBlur(...args);
      }
    };
    const { mask, pipe, defaultValue, placeholderChar, ...other } = this.props; // eslint-disable-line
    // const { mask, pipe, defaultValue, placeholderChar, dontGuide, ...other } = this.props;
    const { input: { value }, readOnly, meta, type } = other; // eslint-disable-line
    return (<TextField onFocus={onFocus} onBlur={onFocusLost} {...other} >
      <MaskedInput
        mask={mask}
        focus={this.state.focus}
        type={type}
        readOnly={readOnly}
        value={value}
        onFocus={onFocus}
        onBlur={onFocusLost}
      /></TextField>);
  }
}

MaskedTextField.propTypes = {
  input: React.PropTypes.any,
  mask: React.PropTypes.any,
  pipe: React.PropTypes.any,
  defaultValue: React.PropTypes.any,
  meta: React.PropTypes.any,
  placeholderChar: React.PropTypes.string,
};

export default MaskedTextField;
