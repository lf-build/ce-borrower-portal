/**
*
* Referral
*
*/

import React from 'react';
import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';
// import { FormattedMessage } from 'react-intl';
// import messages from './messages';

class Referral extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    const ReferralInfo = styled.div`
    font-size: 14px;
    line-height: 22px;
    // text-align: center;
    `;

    const { name, mobile } = this.props;

    return (
      <ReferralInfo>
        {/* To join our referral program, click <a href={`https://www.qbera.com/referral.php?name=${name}&mobile=${mobile}`} style={{ color: '#79cdd5', cursor: 'pointer', textDecoration: 'none' }}>here</a> */}
        <b>Refer your friends and make money!</b>
        <br />
        <br />

        Join our referral program and earn Rs. 2000 or 2% (whichever is higher) of the total loan amount taken by each friend you recommend to Qbera.

        <br />
        <br />
        <div style={{ textAlign: 'center' }}>
          <RaisedButton
            label={'Get Your Unique Referral Link'}
            onClick={() => window.open(`https://www.qbera.com/referral.php?name=${name}&mobile=${mobile}`, '_self')}
            labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
            backgroundColor={'#3663ad'}
            labelColor={'#ffffff'}
          />
        </div>

        {/* <button>Get Your Shareable Unique Referral Link Now</button> */}
      </ReferralInfo>
    );
  }
}

Referral.propTypes = {
  name: React.PropTypes.string,
  mobile: React.PropTypes.string,
};

export default Referral;
