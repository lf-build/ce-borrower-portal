/*
 * Referral Messages
 *
 * This contains all the text for the Referral component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.Referral.header',
    defaultMessage: 'This is the Referral component !',
  },
});
