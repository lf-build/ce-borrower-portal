/**
*
* DateMonthYear
*
*/

import React from 'react';
// import styled from 'styled-components';

import { Field } from 'redux-form/immutable';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import MenuItem from 'material-ui/MenuItem';

class DateMonthYear extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  constructor(props) {
    super(props);
    this.state = {
      date: undefined,
      month: undefined,
      year: undefined,
      showDiv: true,
    };
  }
  componentWillMount() {
    // here in componentWillMount, we will check if the device is a touch device or not
    // so that it will help in handling the touch/click on Aadhaar and Pan Number
    try {
      document.createEvent('TouchEvent');
    } catch (e) {
      this.setState({ showDiv: false });
    }
  }
  render() {
    const getDate = () => {
      const dates = [];
      for (let i = 1; i <= 31; i++) { /* eslint no-plusplus: 0 */
        dates.push(i);
      }
      return dates;
    };

    const getYear = () => {
      const currentYear = new Date().getFullYear();
      const fromYear = currentYear - 70;
      const years = [];
      for (let i = fromYear; i <= currentYear; i++) { /* eslint no-plusplus: 0 */
        years.push(i);
      }
      return years;
    };

    const onTransparentDivTouch = (e) => {
      if (e.type && e.type === 'touchend') {
        this.setState({ showDiv: false });
      }
    };
    return (
      <div style={{ width: '100%' }}>
        <span>
          <Field
            name="date"
            component={SelectField}
            hintText="Date"
            value={this.state.date}
            // onChange={handleDateChange}
            style={{ width: '20%', marginRight: '5%', fontSize: '14px' }}
            hintStyle={{ fontSize: '14px' }}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            maxHeight={200}
            iconStyle={{ height: '24px', width: '24px', paddingRight: '20px' }}
            labelStyle={{ paddingRight: 'none' }}
          >
            {getDate().map((date) => <MenuItem key={date} value={date < 10 ? `0${date}` : `${date}`} primaryText={date} />)}
          </Field>
        </span>

        <span>
          <Field
            name="month"
            component={SelectField}
            hintText="Month"
            value={this.state.date}
            // onChange={handleMonthChange}
            style={{ width: '35%', fontSize: '14px' }}
            hintStyle={{ fontSize: '14px' }}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            maxHeight={200}
            iconStyle={{ height: '24px', width: '24px', paddingRight: '20px' }}
            labelStyle={{ paddingRight: 'none' }}
          >
            <MenuItem value={'01'} primaryText="Jan" />
            <MenuItem value={'02'} primaryText="Feb" />
            <MenuItem value={'03'} primaryText="Mar" />
            <MenuItem value={'04'} primaryText="Apr" />
            <MenuItem value={'05'} primaryText="May" />
            <MenuItem value={'06'} primaryText="June" />
            <MenuItem value={'07'} primaryText="July" />
            <MenuItem value={'08'} primaryText="Aug" />
            <MenuItem value={'09'} primaryText="Sep" />
            <MenuItem value={'10'} primaryText="Oct" />
            <MenuItem value={'11'} primaryText="Nov" />
            <MenuItem value={'12'} primaryText="Dec" />
          </Field>
        </span>

        <span>
          <Field
            name="year"
            component={SelectField}
            hintText="Year"
            value={this.state.date}
            // onChange={handleYearChange}
            style={{ width: '35%', marginLeft: '5%', fontSize: '14px' }}
            hintStyle={{ fontSize: '14px' }}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            maxHeight={200}
            iconStyle={{ height: '24px', width: '24px', paddingRight: '20px' }}
            labelStyle={{ paddingRight: 'none' }}
          >
            {getYear().map((year) => <MenuItem key={year} value={`${year}`} primaryText={year} />)}
          </Field>
        </span>
        <div className={this.state.showDiv ? 'transparent-div' : 'transparent-div hidden'} onTouchTap={(e) => onTransparentDivTouch(e)}></div>
      </div>
    );
  }
}

DateMonthYear.propTypes = {
};

export default DateMonthYear;
