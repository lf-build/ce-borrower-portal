/*
 * VerificationProcess Messages
 *
 * This contains all the text for the VerificationProcess component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.components.VerificationProcess.header',
    defaultMessage: 'This is the VerificationProcess component !',
  },
});
