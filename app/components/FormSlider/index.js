/**
*
* FormSlider
*
*/

import Slider from 'material-ui/Slider';
import createComponent from './createComponent';

const mapError = ({
  meta: { touched, error, warning } = {}, // eslint-disable-line no-unused-vars
  input: { ...inputProps },
  ...props
}) => ({ ...inputProps, ...props });

export default createComponent(
  Slider,
  ({ input: { onDragStart, onChange, ...inputProps }, onChange: onChangeFromField, ...props }) =>  // eslint-disable-line no-unused-vars
    ({
      ...mapError({ ...props, input: inputProps }),
      onChange: (event, value) => {
        onChange(value);
        if (onChangeFromField) {
          onChangeFromField(value);
        }
      },
    })
);
