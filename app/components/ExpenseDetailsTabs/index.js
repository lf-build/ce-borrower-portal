/**
*
* ExpenseDetailsTabs
*
*/

import React from 'react';

const styles = {
  ulStyle: {
    // borderBottom: '5px solid #eaeaea',
    // marginBottom: '15px',
    listStyle: 'none',
    paddingLeft: '0',
    // fontSize: '14px',
    textAlign: 'center',
    fontWeight: 600,
    lineHeight: '16px',
    // textTransform: 'uppercase',
    // fontSize: '11px',
    letterSpacing: '1px',

  },
  inActive: {
    borderBottom: '1px solid #eaeaea',
    paddingBottom: '10px',
    paddingTop: '10px',
  },
  active: {
    borderBottom: '2px solid #79cdd5',
    fontWeight: '700',
    paddingBottom: '10px',
    paddingTop: '10px',
  },
};

class ExpenseDetailsTabs extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { tab } = this.props;
    return (
      <div className={'residence-tabs'}>
        <ul style={styles.ulStyle}>
          <li className={'col-xs-4 col-sm-4'} style={tab === 1 ? styles.active : styles.inActive}>RESIDENCE<br className={'res-tab-br'} /><br className={'res-tab-br'} /></li>
          <li className={'col-xs-4 col-sm-4'} style={tab === 2 ? styles.active : styles.inActive}>CREDIT <br className={'res-tab-br'} /> CARD</li>
          <li className={'col-xs-4 col-sm-4'} style={tab === 3 ? styles.active : styles.inActive}>OTHER <br className={'res-tab-br'} /> EMIs</li>
        </ul>
      </div>
    );
  }
}

ExpenseDetailsTabs.propTypes = {
  tab: React.PropTypes.number.isRequired,
};

export default ExpenseDetailsTabs;
