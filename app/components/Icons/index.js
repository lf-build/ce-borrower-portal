/**
*
* Icons
*
*/

// import React from 'react';
// import styled from 'styled-components';
import styled from 'styled-components';
import svg from './assets/images/ce-slider-option-icons.svg';
import SideBarIconSprint from './assets/images/loan-apply-info-col-icons.png';
import InlineEditSprint from './assets/images/edit-icon-b.png';
import VerificationIconSprint from './assets/images/verification-steps-icons.png';

const VerificationIcons = styled.span`
    background: rgba(0, 0, 0, 0) url(${VerificationIconSprint}) repeat scroll 0 0;
    display: inline-block;
    height: 60px;
    margin-bottom: 5px;
    vertical-align: top;
    width: 60px;
`;

export const BankingIcon = styled(VerificationIcons)`
  background-position: 0px 0px;
`;

export const EducationIcon = styled(VerificationIcons)`
  background-position: 0px -120px;
`;

export const SocialIcon = styled(VerificationIcons)`
  background-position: 0px -60px;
`;

export const EmploymentIcon = styled(VerificationIcons)`
  background-position: 0px -180px;
`;

const Icons = styled.div`
  background: url(${svg}) 0 -360px;
  height: 60px;
  width: 80px;
  margin: 0 auto;
  transform: scale(1.2);
  margin-bottom: 30px;
`;

export const LoanAmountIcon = styled(Icons)`
  background-position: 0 -480px;
`;

const SlideThumbIcons = styled.div`
  background: rgba(0, 0, 0, 0) url(${svg}) no-repeat scroll center 0;
  display: inline-block;
  height: 60px;
  margin-top: 20px;
  position: relative;
  width: 100%;
`;

export const TravelBagIcon = styled(SlideThumbIcons)`
  background-position: center 0px
`;
export const SportsCarIcon = styled(SlideThumbIcons)`
  background-position: center -60px
`;

export const FirstAidBoxIcon = styled(SlideThumbIcons)`
  background-position: center -120px
`;
export const CashFlowIcon = styled(SlideThumbIcons)`
  background-position: center -180px
`;
export const jewelleryIcon = styled(SlideThumbIcons)`
  background-position: center -240px
`;
export const HouseRepairIcon = styled(SlideThumbIcons)`
  background-position: center -300px
`;
export const MobileIcon = styled(SlideThumbIcons)`
  background-position: center -360px
`;
export const MoneyChangeIcon = styled(SlideThumbIcons)`
  background-position: center -420px
`;
export const CashBundleIcon = styled(SlideThumbIcons)`
  background-position: center -480px
`;
export const CheckPassIcon = styled(SlideThumbIcons)`
  background-position: center -540px
`;
export const CheckFailIcon = styled(SlideThumbIcons)`
  background-position: center -600px
`;
export const DangerIcon = styled(SlideThumbIcons)`
  background-position: center -660px
`;
export const HourglassIcon = styled(SlideThumbIcons)`
  background-position: center -720px
`;

const SidebarIcon = styled.div`
    background: url(${SideBarIconSprint}) repeat scroll 0 0;
    height: 20px;
    margin: 0 10px 0 0;
    width: 20px;
    float: left;
`;
export const QuestionMarkIcon = styled(SidebarIcon)`
  background-position: center -0px
`;
export const ShieldIcon = styled(SidebarIcon)`
  background-position: center -20px
`;
export const LockIcon = styled(SidebarIcon)`
  background-position: center -40px
`;
export const CallIcon = styled(SidebarIcon)`
  background-position: center -60px
`;

export const InlineEditIcon = styled.span`
    background: rgba(0, 0, 0, 0) url(${InlineEditSprint}) no-repeat scroll 0 0 / cover;
    display: inline-block;
    height: 15px;
    margin: 0 0 0 2px;
    width: 15px;
    cursor: pointer;
`;
