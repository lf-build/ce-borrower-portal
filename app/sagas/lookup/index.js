import { takeLatest } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { LOOKUP_REQUEST_REQUEST } from './constants';
import * as lookup from './actions';

//  ({ stage, fact, action, payload }) => new Promise((resolve) => setTimeout(() => resolve({ stage, fact, action, payload }), 2000));

function* doLookup(action) {
  const tag = action.meta.tag || (new Date()).getDate().toString();
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'lookup',
    action: action.meta.entity }, {
      tag,
      payload: {
        searchText: action.meta.searchText,
      },
    }))) {
    return;
  }
  yield lookup.lookupStartedFor(tag);
  const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(tag);
  if (valid) {
    yield lookup.lookupFulfilledFor(tag, valid.meta.body);
  } else {
    yield lookup.lookupFailedFor(tag, invalid.meta.body);
  }

  yield lookup.lookupEndedFor(tag);
}

// Individual exports for testing
export function* lookupSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeLatest(LOOKUP_REQUEST_REQUEST, doLookup);
}

// All sagas to be loaded
export default [
  lookupSaga,
];
