import { takeEvery, put, select } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import { fromJS } from 'immutable';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { userInfoAvailable } from './actions';
import { LOAD_PROFILE, USER_SIGNED_OUT } from './constants';

function* loginSaga(action) {
  browserHistory.replace('/user/welcome/direct');
  const { meta: { flow, mobileNumber } } = { ...action };
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'state',
    command: 'restore',
  }, {
    tag: 'state-restore',
    payload: {
      mobileNumber,
      flow,
    },
  }))) {
    return;
  }

  const { valid: persistedResponse } = yield uplink.waitForUplinkExecutionSuccessOrFailure('state-restore');
  if (!persistedResponse) {
    throw new Error('Unable to restore state');
  }
  const persistedState = persistedResponse.meta.body;
  Object.keys(persistedState.state.form).forEach((key) => {
    Object.keys(persistedState.state.form[key].values).forEach((ckey) => {
      if (typeof persistedState.state.form[key].values[ckey] !== 'object' && persistedState.state.form[key].values[ckey] !== null && persistedState.state.form[key].values[ckey] !== undefined) {
        persistedState.state.form[key].values[ckey] = persistedState.state.form[key].values[ckey].toString();
      }
    });
  });
  if (persistedState.state.form.employment) {
    persistedState.state.form.employment.values.workExperience = parseInt(persistedState.state.form.employment.values.workExperience, 10);
  }


  // Slider expects a number :(
  persistedState.state.form.amount.values.amount = parseInt(persistedState.state.form.amount.values.amount, 10);
  yield put({
    type: 'RESET',
    state: fromJS({ auth: yield select((state) => state.get('auth') && state.get('auth').toJS()), ...persistedState.state }),
  });


  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'who-am-i',
    uplinkId: persistedState.workflowId,
  }, {
    tag: 'who-am-i',
  }))) {
    return;
  }

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('who-am-i');
  if (valid) {
    yield put(userInfoAvailable(valid.meta.body));
  } else {
    throw new Error('Invalid token');
  }
}

function* logoutSaga() {
  browserHistory.replace('/user/welcome/direct');
  if (!(yield uplink.requestUplinkExecution({
    dock: 'authorization',
    section: 'identity',
    command: 'logout',
  }, {
    tag: 'sign-out',
  }))) {
    return;
  }
  yield uplink.waitForUplinkExecutionSuccessOrFailure('sign-out');
  // if (valid) {
  yield put({
    type: 'RESET',
    state: undefined,
  });
  localStorage.clear();
  // browserHistory.replace('/');
  window.location.assign('http://www.qbera.com/');
  // } else {
  //   window.location.assign('http://www.qbera.com/');
  //   throw new Error('Invalid token');
  // }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(LOAD_PROFILE, loginSaga);
  yield takeEvery(USER_SIGNED_OUT, logoutSaga);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
