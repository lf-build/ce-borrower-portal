/*
 *
 * SignInPage actions
 *
 */

import {
  DEFAULT_ACTION,
  USER_SIGNED_IN,
  USER_SIGNED_OUT,
  USER_INFO_AVAILABLE,
  LOAD_PROFILE,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function userSignedIn(token, username) {
  return {
    type: USER_SIGNED_IN,
    meta: {
      token,
      username,
    },
  };
}

export function loadUserProfile(meta) {
  return {
    type: LOAD_PROFILE,
    meta,
  };
}

export function userInfoAvailable(user) {
  return {
    type: USER_INFO_AVAILABLE,
    meta: {
      user,
    },
  };
}

export function userSignedOut() {
  return {
    type: USER_SIGNED_OUT,
  };
}
