/*
 *
 * auth reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // USER_SIGNED_OUT,
  USER_SIGNED_IN,
  USER_INFO_AVAILABLE,
} from './constants';

const initialState = fromJS({});

function authReducer(state = initialState, action) {
  switch (action.type) {
    case USER_SIGNED_IN:
      return state
      .set('token', action.meta.token)
      .set('username', action.meta.username);
    case USER_INFO_AVAILABLE:
      return state
      .set('user', action.meta.user);
    // case USER_SIGNED_OUT:
    //   return state
    //   .set('user', undefined)
    //   .set('token', undefined);
    default:
      return state;
  }
}

export default authReducer;
