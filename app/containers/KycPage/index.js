/*
 *
 * KycPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import RadioButtonGroup from 'redux-form-material-ui/lib/RadioButtonGroup';
import { Field, formValueSelector } from 'redux-form/immutable';
import { RadioButton } from 'material-ui/RadioButton';

import CheckBoxOn from 'material-ui/svg-icons/toggle/check-box';
import CheckBoxOff from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import { createStructuredSelector } from 'reselect';

import FileDropZone from '../../components/FileDropZone';
import {
  fileRecieved as fileRecievedActionCreator,
  fetchPendingDocumentListRequest as fetchPendingDocumentListRequestActionCreator,
} from './actions';

import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import makeSelectKycPage from './selectors';

const KycPageFormSelector = formValueSelector('kyc');

export class KycPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { fetchPendingDocuments } = this.props;
    if (fetchPendingDocuments) {
      fetchPendingDocuments();
    }
  }
  render() {
    const styles = {
      radioButton: {
        width: '100%',
        marginTop: '5px',
        // marginRight: '30px',
        padding: '10px 5px',
        color: '#4e4e4e',
        border: '1px solid lightblue',
        borderRadius: '5px',
        backgroundColor: 'rgba(30, 168, 221, 0.1)',
      },
      iconStyle: {
        marginRight: '2px',
        color: '#4e4e4e',
      },
      labelStyle: {
        color: '#5e5e5e',
        fontSize: '14px',
      },
    };
    const { fileRecieved, kycDocument } = this.props;
    const { files, pendingDocuments } = this.props.KycPage;
    const pendingDocumentNames = (pendingDocuments || []).map((d) => d.documentName[0]);
    const handleFileDropped = (stipulation) => (e) => fileRecieved({ ...e, ...stipulation, ...{ stipulationType: stipulation.documentName[0], documentName: undefined } });

    const buildPayload = () => ({
      documents: Object.keys(files)
        .filter((doc) => pendingDocumentNames.includes(doc))
        .map((stipulationType) => files[stipulationType])
        .filter((doc) => doc)
        .map((doc) => doc[0]),
    });
    return (
      <LoanStep payloadGenerator={buildPayload} name={'kyc'} title={messages.header}>
        <span style={{ fontSize: 14, color: '#76cdd6' }}>Verification method</span>
        <Field name="kycDocument" validate={(value) => value ? undefined : 'Please let us know if you have Credit Card'} component={RadioButtonGroup}>
          <RadioButton
            value={'true'}
            label="Upload documents"
            checkedIcon={<CheckBoxOn />}
            uncheckedIcon={<CheckBoxOff />}
            labelStyle={styles.labelStyle}
            iconStyle={styles.iconStyle}
            style={styles.radioButton}
          />
          <RadioButton
            value={'false'}
            labelStyle={styles.labelStyle}
            checkedIcon={<CheckBoxOn />}
            uncheckedIcon={<CheckBoxOff />}
            label="Schedule a visit for document pick-up"
            iconStyle={styles.iconStyle}
            style={styles.radioButton}
          />
        </Field>

        <div className={kycDocument === 'true' ? '' : 'hidden'}>
          <p>
            <span> Please provide following documents:
              </span>
          </p>
          <div >
            { pendingDocuments ? pendingDocuments.filter((doc) => doc.documentName.length)
                            .map((doc) => (
                              <FileDropZone
                                key={doc.documentName[0]}
                                onDrop={handleFileDropped(doc)}
                                files={files[doc.documentName[0]] || []}
                                message={`Drop "${(messages[doc.documentName[0]] || { defaultMessage: doc.documentName[0] }).defaultMessage}" here or click here to open browse box`}
                                accept={'application/pdf,image/*'}
                              />
                            ))

                          : <span />
          }


          </div>
          <span style={{ fontSize: '10px' }}>(* file Should be not more than 2MB and Format – PNG/JPEG/PDF)</span>
        </div>
      </LoanStep>
    );
  }
}

KycPage.propTypes = {
  KycPage: PropTypes.object,
  fileRecieved: PropTypes.func,
  fetchPendingDocuments: PropTypes.func,
  kycDocument: PropTypes.string,
};

const mapStateToProps = createStructuredSelector({
  KycPage: makeSelectKycPage(),
  kycDocument: (state) => KycPageFormSelector(state, 'kycDocument'),
});

function mapDispatchToProps(dispatch) {
  return {
    fileRecieved: ({ file, dataUrl, type, factName, methodName, stipulationType }) => dispatch(fileRecievedActionCreator({ file, dataUrl, type, factName, methodName, stipulationType })),
    fetchPendingDocuments: () => dispatch(fetchPendingDocumentListRequestActionCreator()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(KycPage);
