import { createSelector } from 'reselect';

/**
 * Direct selector to the kycPage state domain
 */
const selectKycPageDomain = () => (state) => state.get('kycPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by KycPage
 */

const makeSelectKycPage = () => createSelector(
  selectKycPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectKycPage;
export {
  selectKycPageDomain,
};
