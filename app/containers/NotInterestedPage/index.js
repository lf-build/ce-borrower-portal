/*
 *
 * NotInterestedPage
 *
 */

import React from 'react';
import styled from 'styled-components';

import messages from './messages';
import LoanStep from '../LoanStep';
import { CheckPassIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

export class NotInterestedPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep navigationPanel={() => <span />} title={messages.header}>
        <CheckPassIcon />
        <CongratulationText />
        <CongratulationText>
          We are sorry to see you go.
        </CongratulationText>
      </LoanStep>
    );
  }
}

NotInterestedPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = createStructuredSelector({
//   NotInterestedPage: makeSelectNotInterestedPage(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

export default NotInterestedPage;
