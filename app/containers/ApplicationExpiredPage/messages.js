/*
 * ApplicationExpiredPage Messages
 *
 * This contains all the text for the ApplicationExpiredPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationExpiredPage.header',
    defaultMessage: 'Application Expired',
  },
});
