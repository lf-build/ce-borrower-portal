/*
 *
 * ApplicationExpiredPage
 *
 */

import React from 'react';
// import { connect } from 'react-redux';
// import Helmet from 'react-helmet';
import styled from 'styled-components';
// import { createStructuredSelector } from 'reselect';
// import makeSelectApplicationExpiredPage from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { CheckPassIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

export class ApplicationExpiredPage extends React.PureComponent { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep navigationPanel={() => <span />} title={messages.header}>
        <CheckPassIcon />
        <CongratulationText>
          Your loan application is expired. Please contact us on 1800 4198 121 (toll-free) or e-mail us at contact@qbera.com
        </CongratulationText>
      </LoanStep>
    );
  }
}

ApplicationExpiredPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

// const mapStateToProps = createStructuredSelector({
//   ApplicationExpiredPage: makeSelectApplicationExpiredPage(),
// });

// function mapDispatchToProps(dispatch) {
//   return {
//     dispatch,
//   };
// }

export default ApplicationExpiredPage;
