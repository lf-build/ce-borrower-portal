/*
 * OtherExpenseDetailsPage Messages
 *
 * This contains all the text for the OtherExpenseDetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.OtherExpenseDetailsPage.header',
    defaultMessage: 'A little about expenses',
  },
});
