
import { fromJS } from 'immutable';
import applicationUnderReviewReducer from '../reducer';

describe('applicationUnderReviewReducer', () => {
  it('returns the initial state', () => {
    expect(applicationUnderReviewReducer(undefined, {})).toEqual(fromJS({}));
  });
});
