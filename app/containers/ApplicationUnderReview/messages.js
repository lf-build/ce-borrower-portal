/*
 * ApplicationUnderReview Messages
 *
 * This contains all the text for the ApplicationUnderReview component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationUnderReview.header',
    defaultMessage: '',
  },
});
