import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationProgress state domain
 */
const selectApplicationProgressDomain = () => (state) => state.get('applicationProgress');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationProgress
 */

const makeSelectApplicationProgress = () => createSelector(
  selectApplicationProgressDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationProgress;
export {
  selectApplicationProgressDomain,
};
