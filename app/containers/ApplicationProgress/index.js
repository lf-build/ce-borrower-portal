/*
 *
 * ApplicationProgress
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import {
//   Step,
//   Stepper,
//   StepButton,
//   // StepContent,
// } from 'material-ui/Stepper';
import styled from 'styled-components';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectApplicationProgress from './selectors';
// import messages from './messages';

// const isInLoadDetailsStep = (index) => index === 0;
// const isInPersonalInformationStep = (index) => index === 1;
// const isInVerificationAndRateCheckStep = (index) => index === 2;
// const isInLoanOfferStep = (index) => index === 3;

export class ApplicationProgress extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { ApplicationProgress: ApplicationProgressState, orientation, ...otherParams } = this.props;
    // const { level1, level2, level3 } = ApplicationProgressState;
    const { level1, level2 } = ApplicationProgressState;
    const { mlevel1, mlevel2 } = ApplicationProgressState;
    // if (!mlevel1) {
    //   mlevel1 = level1;
    // }
    // if (!mlevel2) {
    //   mlevel2 = level2;
    // }
    // const stepIndex = level1 || 0;
    const getClassForLevel1 = (level, currentLevel1) => {
      if (level === currentLevel1) {
        return 'active';
      } else if (level < currentLevel1) {
        return 'completed';
      }

      return 'inactive';
    };

    const getClassForLevel2 = (level, currentLevel1, l2, currentLevel2, ml1, ml2) => {
      // Active/inactive logic.
      if (level === currentLevel1 && l2 === currentLevel2) {
        return 'active';
      }

      // Completed logic.
      if (level === ml1) {
        if (l2 <= ml2) {
          return 'completed';
        }
        return 'disable';
      }

      return '';
    };

    const getLevelForPersonalInfo = (l2) => {
      if (l2 === 8 || l2 === 9) {
        return 6;
      }

      return l2;
    };

    const getClassNameForMobileView = (level, currentLevel) => {
      if (level === currentLevel) {
        // if (level === 3) {
        //   return 'progress-last';
        // }
        return 'is-active';
      } else if (level < currentLevel) {
        return 'is-complete';
      }

      return '';
    };

    const Label = styled.label``;
    const A = styled.a``;

    // const orientation = 'vertical'; // horizontal
    if (orientation === 'horizontal') {
      return (
        <div {...otherParams}>
          <div className="col-md-3 no-padd">
            <div className="heading">
              <div className="mobile-nav">

                <ol className="progress">
                  <li className={getClassNameForMobileView(0, level1)} data-step="1">
                Loan Details
              </li>
                  <li className={getClassNameForMobileView(1, level1)} data-step="2">
                Personal Information
              </li>
                  <li className={getClassNameForMobileView(2, level1)} data-step="3">
                Verification
              </li>
                  <li data-step="4" className={getClassNameForMobileView(3, level1)}>
                Loan Offer
              </li>
                </ol>

              </div>
            </div>
          </div>
        </div>
      );
    }
    return (
      <div className="loan-steps-container">
        <ul className="progress-bar">
          <li className={`stop step${mlevel1}-${mlevel1 === 1 ? getLevelForPersonalInfo(mlevel2) : mlevel2}`}></li>
          {/* here you need to make the "step2" dynamic based on the step we are currently*/}
          <li className="stop last" style={{ display: level1 === 3 && level2 === 1 ? 'none' : 'block' }} ></li>
        </ul>
        <ul className="loan-steps">
          <li className={getClassForLevel1(0, mlevel1)}>
            <div className="step-number">1</div>
            <div className="menu-items">
              <Label>Loan Details</Label>
              <ul>
                <li className={getClassForLevel2(0, level1, 0, level2, mlevel1, mlevel2)}><A>Loan Amount</A></li>
                <li className={getClassForLevel2(0, level1, 1, level2, mlevel1, mlevel2)}><A>Loan Purpose</A></li>
              </ul>
            </div>
          </li>
          <li className={getClassForLevel1(1, mlevel1)}>
            <div className="step-number">2</div>
            <div className="menu-items">
              <Label>Personal Information</Label>
              <ul>
                <li className={getClassForLevel2(1, level1, 0, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Profile</A></li>
                <li className={getClassForLevel2(1, level1, 1, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Account Creation</A></li>
                <li className={getClassForLevel2(1, level1, 2, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Employment Details</A></li>
                <li className={getClassForLevel2(1, level1, 3, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Expenses</A></li>
                <li className={getClassForLevel2(1, level1, 4, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Identification & Residential Details</A></li>
                <li className={getClassForLevel2(1, level1, 5, getLevelForPersonalInfo(level2), mlevel1, mlevel2)}><A>Review</A></li>
              </ul>
            </div>
          </li>
          <li className={getClassForLevel1(2, mlevel1)}>
            <div className="step-number">3</div>
            <div className="menu-items">
              <Label>Verification &amp; Rate Check</Label>
              <ul>
                <li className={getClassForLevel2(2, level1, 0, level2, mlevel1, mlevel2)}><A>Banking Verification</A></li>
                <li className={getClassForLevel2(2, level1, 1, level2, mlevel1, mlevel2)}><A>Social Verification</A></li>
                <li className={getClassForLevel2(2, level1, 2, level2, mlevel1, mlevel2)}><A>Education Details</A></li>
                <li className={getClassForLevel2(2, level1, 3, level2, mlevel1, mlevel2)}><A>Employment Verification</A></li>
              </ul>
            </div>
          </li>
          <li className={getClassForLevel1(3, mlevel1)}>
            <div className="step-number">4</div>
            <div className="menu-items">
              <Label>Loan Offer</Label>
              <ul>
                <li className={getClassForLevel2(3, level1, 0, level2, mlevel1, mlevel2)}><A>Loan Options</A></li>
                {/* <li className={getClassForLevel2(3, level1, 1, level2)}><A>KYC</A></li>*/}
              </ul>
            </div>
          </li>
        </ul>
      </div>
    );
  }
}

ApplicationProgress.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  ApplicationProgress: PropTypes.object,
  orientation: PropTypes.string.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ApplicationProgress: makeSelectApplicationProgress(),
});

function mapDispatchToProps() { // dispatch) {
  return {
    // dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationProgress);
