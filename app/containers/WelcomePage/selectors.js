import { createSelector } from 'reselect';

/**
 * Direct selector to the welcomePage state domain
 */
const selectWelcomePageDomain = () => (state) => state.get('welcomePage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by WelcomePage
 */

const makeSelectWelcomePage = () => createSelector(
  selectWelcomePageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectWelcomePage;
export {
  selectWelcomePageDomain,
};
