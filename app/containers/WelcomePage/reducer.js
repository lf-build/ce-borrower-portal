/*
 *
 * WelcomePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  UTM_PARAM_FOUND,
} from './constants';

const initialState = fromJS({
  referralPage: undefined,
  utmSource: undefined,
});
// utm_medium, utm_source, utm_campaign, utm_term and utm_content capturing 5 utm
function welcomePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case UTM_PARAM_FOUND:
      return state.set('referralPage', action.payload.referralPage)
        .set('utmSource', action.payload.utmSource)
        .set('utmMedium', action.payload.utmMedium)
        .set('utmCampaign', action.payload.utmCampaign)
        .set('utmTerm', action.payload.utmTerm)
        .set('utmContent', action.payload.utmContent)
        .set('gclid', action.payload.gclid);
    default:
      return state;
  }
}

export default welcomePageReducer;
