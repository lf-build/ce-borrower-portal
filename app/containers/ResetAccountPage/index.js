/*
 *
 * ResetAccountPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import RaisedButton from 'material-ui/RaisedButton';
import { withRouter } from 'react-router';

import makeSelectResetAccountPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';

const shortDescription = () => <p> Please select a password to save and retrieve your application. </p>;

export class ResetAccountPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    if (!this.props.params) {
      return <span />;
    }
    const { params: { token, username } } = this.props;

    const buildPayload = () => ({
      token,
      username,
    });
    return (
      <LoanStep
        payloadGenerator={buildPayload}
        saveErrorClass={'reset-account'}
        name={'reset-account'}
        title={messages.header}
        ShortDescription={shortDescription}
        reduceWidth
        navigationPanel={(props) => <RaisedButton disabled={props.isLoadingOrSubmiting} onTouchTap={props.handleGoNext} primary label="Submit" />}
      >
        <TextField
          name={'password'}
          hintText="Password"
          floatingLabelText="Password"
          type={'password'}
          fullWidth
        />
        <TextField
          name={'confirmPassword'}
          hintText="Confirm Password"
          floatingLabelText="Confirm Password"
          type={'password'}
          fullWidth
        />
      </LoanStep>
    );
  }
}

ResetAccountPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  params: React.PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  ResetAccountPage: makeSelectResetAccountPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(ResetAccountPage));
