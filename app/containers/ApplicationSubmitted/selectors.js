import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationSubmitted state domain
 */
const selectApplicationSubmittedDomain = () => (state) => state.get('applicationSubmitted');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationSubmitted
 */

const makeSelectApplicationSubmitted = () => createSelector(
  selectApplicationSubmittedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationSubmitted;
export {
  selectApplicationSubmittedDomain,
};
