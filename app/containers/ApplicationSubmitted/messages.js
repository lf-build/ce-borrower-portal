/*
 * ApplicationSubmitted Messages
 *
 * This contains all the text for the ApplicationSubmitted component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationSubmitted.header',
    defaultMessage: '',
  },
});
