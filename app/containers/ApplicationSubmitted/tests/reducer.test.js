
import { fromJS } from 'immutable';
import applicationSubmittedReducer from '../reducer';

describe('applicationSubmittedReducer', () => {
  it('returns the initial state', () => {
    expect(applicationSubmittedReducer(undefined, {})).toEqual(fromJS({}));
  });
});
