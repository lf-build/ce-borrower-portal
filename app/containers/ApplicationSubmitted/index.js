/*
 *
 * ApplicationSubmitted
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import messages from './messages';
import makeSelectApplicationSubmitted from './selectors';
import LoanStep from '../LoanStep';
import { CheckPassIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

export class ApplicationSubmitted extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep navigationPanel={() => <span />} title={messages.header}>
        <CheckPassIcon />
        <CongratulationText />
        <CongratulationText>
          Your application is complete. We will contact you shortly.
        </CongratulationText>
      </LoanStep>
    );
  }
}

ApplicationSubmitted.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ApplicationSubmitted: makeSelectApplicationSubmitted(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationSubmitted);
