/*
 *
 * EmploymentPage actions
 *
 */

import {
  DEFAULT_ACTION,
  GET_FORM26AS_STATUS_REQUEST,
  GET_FORM26AS_STATUS_REQUEST_STARTED,
  GET_FORM26AS_STATUS_REQUEST_FULFILLED,
  GET_FORM26AS_STATUS_REQUEST_FAILED,
  GET_FORM26AS_STATUS_REQUEST_ENDED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_STARTED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FULFILLED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FAILED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_ENDED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function getForm26ASStatusRequest() {
  return {
    type: GET_FORM26AS_STATUS_REQUEST,
  };
}

export function getForm26ASStatusRequestStarted() {
  return {
    type: GET_FORM26AS_STATUS_REQUEST_STARTED,
  };
}

export function getForm26ASStatusRequestFulfilled(status) {
  return {
    type: GET_FORM26AS_STATUS_REQUEST_FULFILLED,
    meta: {
      status,
    },
  };
}

export function getForm26ASStatusRequestFailed(error) {
  return {
    type: GET_FORM26AS_STATUS_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getForm26ASStatusRequestEnded() {
  return {
    type: GET_FORM26AS_STATUS_REQUEST_ENDED,
  };
}

export function getVerificationEmailSentStatusRequest() {
  return {
    type: GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST,
  };
}

export function getVerificationEmailSentStatusRequestStarted() {
  return {
    type: GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_STARTED,
  };
}

export function getVerificationEmailSentStatusRequestFulfilled(status) {
  return {
    type: GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FULFILLED,
    meta: {
      status,
    },
  };
}

export function getVerificationEmailSentStatusRequestFailed(error) {
  return {
    type: GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getVerificationEmailSentStatusRequestEnded() {
  return {
    type: GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_ENDED,
  };
}
