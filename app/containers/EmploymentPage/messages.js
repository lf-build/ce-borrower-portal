/*
 * EmploymentPage Messages
 *
 * This contains all the text for the EmploymentPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.EmploymentPage.header',
    defaultMessage: 'Please complete the verification process to finalise your interest rate',
  },
  Form26AS: {
    id: 'app.containers.KycPage.Form26AS',
    defaultMessage: 'Form-26 AS',
  },
});
