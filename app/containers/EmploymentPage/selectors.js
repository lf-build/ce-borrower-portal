import { createSelector } from 'reselect';

/**
 * Direct selector to the employmentPage state domain
 */
const selectEmploymentPageDomain = () => (state) => state.get('employmentPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EmploymentPage
 */

const makeSelectEmploymentPage = () => createSelector(
  selectEmploymentPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEmploymentPage;
export {
  selectEmploymentPageDomain,
};
