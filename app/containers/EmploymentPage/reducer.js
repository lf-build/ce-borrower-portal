/*
 *
 * EmploymentPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FILE_RECIEVED,
  FILE_REMOVED,
  FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED,
} from '../KycPage/constants';

import {
  GET_FORM26AS_STATUS_REQUEST_FULFILLED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FULFILLED,
  GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FAILED,
  SET_EMP_YM_ERROR_MSG,
} from './constants';

const initialState = fromJS({
  files: {},
  pendingDocs: [],
});

function employmentPageReducer(state = initialState, action) {
  switch (action.type) {
    case FILE_RECIEVED:
      return state.set('files', { ...state.get('files'), ...{ [action.meta.stipulationType]: [action.payload] } });
    case FILE_REMOVED:
      return state.set('files', { ...state.get('files'), ...{ [action.meta.stipulationType]: undefined } });
    case FETCH_PENDING_DOCUMENT_LIST_REQUEST_FUILFILLED:
      return state.set('pendingDocuments', action.meta);
    case GET_FORM26AS_STATUS_REQUEST_FULFILLED:
      return state.set('isForm26ASAvailable', action.meta.status);
    case GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FULFILLED:
      return state.set('isEmailSent', action.meta.status);
    case GET_VERIFICATION_EMAIL_SENT_STATUS_REQUEST_FAILED:
      return state.set('isEmailSent', false);
    case SET_EMP_YM_ERROR_MSG:
      return state.set('errorYMMsg', action.payload.errorMsg);
    default:
      return state;
  }
}

export default employmentPageReducer;
