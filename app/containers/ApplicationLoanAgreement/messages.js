/*
 * ApplicationLoanAgreement Messages
 *
 * This contains all the text for the ApplicationLoanAgreement component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ApplicationLoanAgreement.header',
    defaultMessage: 'Congratulations!',
  },
});
