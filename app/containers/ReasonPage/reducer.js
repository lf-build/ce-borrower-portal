/*
 *
 * ReasonPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SET_PURPOSE,
} from './constants';

const initialState = fromJS({});

function reasonPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SET_PURPOSE:
      return state
             .set('selected', action.purpose);
    default:
      return state;
  }
}

export default reasonPageReducer;
