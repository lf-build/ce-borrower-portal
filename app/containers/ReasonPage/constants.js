/*
 *
 * ReasonPage constants
 *
 */

export const DEFAULT_ACTION = 'app/ReasonPage/DEFAULT_ACTION';
export const SET_PURPOSE = 'app/ReasonPage/SET_PURPOSE';
