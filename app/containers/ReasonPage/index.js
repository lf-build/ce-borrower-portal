/*
 *
 * ReasonPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form/immutable';
// import MUIAutoComplete from 'material-ui/AutoComplete';
// import AutoComplete from 'redux-form-material-ui/lib/AutoComplete';
import { createStructuredSelector } from 'reselect';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import MenuItem from 'material-ui/MenuItem';
import styled from 'styled-components';


import TextField from '../../components/RequiredTextField';
import messages from './messages';
import { setPurpose as setPurposeAction } from './actions';
import LoanStep from '../../components/LoanStepForm';
import makeSelectReasonPage from './selectors';
import makeAppSelect from '../App/selectors';

const OtherPurposeLink = styled.span`
  color: #79cdd5;
  cursor: pointer;
  text-align: center;
`;
export class ReasonPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const otherLoanPurposeSource = [
      'business',
      'education',
      'assetacquisition',
      'agriculture',
      'other',
    ];
    const { reasonPage: form, setPurpose, setDDValue } = this.props;
    const { selected } = form;
    // const requiredValidation = (value) => {
    //   if (selected === 'other') {
    //     if (!value) {
    //       return 'Please let us know the purpose for your loan';
    //     }
    //   }
    //   return undefined;
    // };

    const handlePurposeSelect = (purpose, ignoreDD) => () => {
      setPurpose({ purpose });

      // If current selected is other purpose
      if (otherLoanPurposeSource.includes(selected)) {
        // If ignoreDD is undefined,
        if (!ignoreDD) {
          // If not ignore, set dd
          setDDValue({ purpose });
        }
      } else {
        // if selected is not OTHER, then update dd.
        setDDValue({ purpose });
      }
    };

    const purposes = [
      {
        id: 'travel',
        icon: '',
        label: 'Travel',
        showInGrid: true,
      },
      {
        id: 'vehiclepurchase',
        icon: 'icon2',
        label: 'Vehicle Purchase',
        showInGrid: true,
      },
      {
        id: 'medical',
        icon: 'icon3',
        label: 'Medical',
        showInGrid: true,
      },
      {
        id: 'loanrefinancing',
        icon: 'icon4',
        label: 'Transfer Existing Loan',
        showInGrid: true,
      },
      {
        id: 'wedding',
        icon: 'icon5',
        label: 'Wedding',
        showInGrid: true,
      },
      {
        id: 'homeimprovement',
        icon: 'icon6',
        label: 'Home Improvement',
        showInGrid: true,
      }];

    const SlideThumbBlock = styled.div``;
    const PurposeLabel = styled.label`
      text-transform: uppercase;
      font-size: 11px;
    `;
    const { App: { reviewPage } } = this.props;
    const required = (value) => !value ? 'Please let us know the purpose of your loan' : undefined;

    return (
      <LoanStep noBorder fullWidth simulateCurrentRoute={reviewPage} nextButtonName={reviewPage && 'Go to review'} noBack={reviewPage !== undefined} payloadGenerator={({ reasonText }) => ({ reason: selected, reasonText: selected === 'other' ? reasonText : undefined })} name={'reason'} title={messages.header} noPadding>
        <div className="ce-loan-apply-slide" style={{ marginTop: '20px' }}>
          <div className="slide-loan-perpose">
            <div className="slide-thumbs">
              {
              purposes
                 .filter((purpose) => purpose.showInGrid)
                 .map((purpose, index) =>
                   <SlideThumbBlock onClick={handlePurposeSelect(purpose.id)} key={`thumb-div-${index}`} className={`slide-thumb ${purpose.id === selected ? 'selected' : ''}`}>
                     <div key={`thumb-div-icon-${index}`} className="slide-thumb-icon">
                       <div key={`thumb-div-icon-bg-${index}`} className="slide-thumb-bg"></div>
                       <span key={`thumb-div-icon-actual-${index}`} className={`icon ${purpose.icon}`}></span>
                     </div>
                     <div className="slide-thumb-text">
                       <PurposeLabel>{purpose.label}</PurposeLabel>
                     </div>
                   </SlideThumbBlock>
                   )
            }
            </div>
          </div>

          <div className="center-block" style={{ marginTop: 20 }}>
            <OtherPurposeLink onClick={handlePurposeSelect('business', true)}>
              <span
                id="otherPurposeLink"
                onMouseEnter={() => { document.getElementById('otherPurposeLink').style.borderBottom = '1px solid'; }}
                onMouseLeave={() => { document.getElementById('otherPurposeLink').style.borderBottom = 'none'; }}
              >
                Any other purpose?
              </span>
            </OtherPurposeLink>
          </div>

          {/* <Field
            validate={requiredValidation}
            className={selected !== 'other' ? 'hidden' : ''}
            name="reasonText"
            component={AutoComplete}
            floatingLabelText="Other"
            openOnFocus
            filter={MUIAutoComplete.fuzzyFilter}
            onNewRequest={(value) => {
              console.log('AutoComplete ', value); // eslint-disable-line no-console
            }}

            dataSource={otherLoanPurposeSource}
          />*/}
        </div>
        <div className={'col-md-6 col-sm-6 col-sm-offset-3'}>
          <Field
            className={otherLoanPurposeSource.includes(selected) ? '' : 'hidden'}
            name="reason"
            hintText="Select"
            floatingLabelStyle={{ fontSize: '12px' }}
            floatingLabelFocusStyle={{ fontSize: '14px' }}
            floatingLabelShrinkStyle={{ fontSize: '14px' }}
            component={SelectField}
            validate={required}
            fullWidth
            selectedMenuItemStyle={{ color: '#79cdd5' }}
            onChange={(e) => {
              handlePurposeSelect(Object.keys(e).filter((a) => a !== 'preventDefault').map((a) => e[a]).join(''))();
            }}
          >
            <MenuItem value={'business'} primaryText="Business" />
            <MenuItem value={'education'} primaryText="Education" />
            <MenuItem value={'assetacquisition'} primaryText="Asset Acquisition" />
            <MenuItem value={'agriculture'} primaryText="Agriculture" />
            <MenuItem value={'other'} primaryText="Other" />
          </Field>
        </div>
        <div className={'col-md-6 col-sm-6 col-sm-offset-3'}>
          <TextField
            className={selected !== 'other' ? 'hidden' : ''}
            optional={selected !== 'other'}
            minLen={selected !== 'other' ? 1 : 2}
            name="reasonText"
            fullWidth
            floatingLabelText="Other Purpose"
          />
        </div>
      </LoanStep>
    );
  }
}

ReasonPage.propTypes = {
  reasonPage: PropTypes.object,
  setPurpose: PropTypes.func.isRequired,
  setDDValue: PropTypes.func.isRequired,
  App: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  reasonPage: makeSelectReasonPage(),
  App: makeAppSelect(),
});

// const mapStateToProps = (state) => ({ form: ReasonPageFormSelector(state) });

function mapDispatchToProps(dispatch) {
  return {
    setPurpose: ({ purpose }) => dispatch(setPurposeAction({ purpose })),
    setDDValue: ({ purpose }) => dispatch({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'reason',
        field: 'reason',
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: purpose,
    }),

  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ReasonPage);
