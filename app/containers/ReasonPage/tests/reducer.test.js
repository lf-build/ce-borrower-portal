
import { fromJS } from 'immutable';
import reasonPageReducer from '../reducer';

describe('reasonPageReducer', () => {
  it('returns the initial state', () => {
    expect(reasonPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
