import { createSelector } from 'reselect';

/**
 * Direct selector to the reasonPage state domain
 */
const selectReasonPageDomain = () => (state) => state.get('reasonPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ReasonPage
 */

const makeSelectReasonPage = () => createSelector(
  selectReasonPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectReasonPage;
export {
  selectReasonPageDomain,
};
