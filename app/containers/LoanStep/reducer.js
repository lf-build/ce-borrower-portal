/*
 *
 * LoanStep reducer
 *
 */

import { fromJS } from 'immutable';
// import {
//   DEFAULT_ACTION,
// } from './constants';

import {
  // DISABLE_ROUTE,
  // NAVIGATE_NEXT_REQUEST,
  NAVIGATE_NEXT_REQUEST_STARTED,
  // NAVIGATE_NEXT_REQUEST_FULFILLED,
  NAVIGATE_NEXT_REQUEST_ABORTED,
  NAVIGATE_NEXT_REQUEST_ENDED,
} from '../NavigationHelper/constants';

import {
  CLEAR_SAVE_FAILED,
} from './constants';

const initialState = fromJS({});

function loanStepReducer(state = initialState, action) {
  switch (action.type) {
    case CLEAR_SAVE_FAILED:
      return state.set('saveFailed', false);
    case NAVIGATE_NEXT_REQUEST_STARTED:
      return state
      .set('loading', true)
      .set('saveFailed', false)
      .set('validationFailed', false);
    case NAVIGATE_NEXT_REQUEST_ABORTED:
      return state
      .set('saveFailed', action.meta.failedToSave)
      .set('validationFailed', action.meta.failedToValidate);
    case NAVIGATE_NEXT_REQUEST_ENDED:
      return state
      .set('loading', false);
    default:
      return state;
  }
}

export default loanStepReducer;
