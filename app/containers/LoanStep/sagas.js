import { takeEvery, put } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';

import {
  SAVE_LOANSTEP_REQUEST,
  SAVE_LOANSTEP_EXECUTION_DENIED,
  SAVE_LOANSTEP_STARTED,
  SAVE_LOANSTEP_FULFILLED,
  SAVE_LOANSTEP_FAILED,
  SAVE_LOANSTEP_ENDED,
} from './constants';

const baseUplinkAction = {
  dock: 'on-boarding',
};

const workflowMap = {
  amount: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'opportunity',
      command: 'set-amount',
    }),
  },
  reason: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'opportunity',
      command: 'set-reason',
    }),
  },
  'basic-information': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'opportunity',
      command: 'set-basic-information',
    }),
  },
  mobile: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'sign-up',
      command: 'set-mobile',
    }),
  },
  otp: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'sign-up',
      command: 'verify-otp',
    }),
  },
  'create-account': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'sign-up',
      command: 'create-account',
    }),
  },
  'work-details': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'qualify',
      command: 'set-work-details',
    }),
  },
  'residence-expense-details': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'qualify',
      command: 'set-residence-expenses',
    }),
  },
  'credit-card-expense-details': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'qualify',
      command: 'set-credit-card-expenses',
    }),
  },
  'other-expense-details': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'qualify',
      command: 'set-other-expenses',
    }),
  },
  'personal-details': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'qualify',
      command: 'set-details',
    }),
  },
  'review-application': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'application',
      command: 'submit',
    }),
  },
  'initial-offer': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'offer',
      command: 'select-initial-offer',
    }),
  },
  banking: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'verify',
      command: 'set-bank',
    }),
  },
  education: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'verify',
      command: 'set-education',
    }),
  },
  employment: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'verify',
      command: 'set-employment',
    }),
  },
  social: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'verify',
      command: 'set-social',
    }),
  },
  'final-offer': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'offer',
      // command: 'generate-final',
      command: 'select-final-offer',
    }),
  },
  kyc: {
    save: uplink.requestUplinkExecution.bind(undefined, {
      ...baseUplinkAction,
      section: 'kyc',
      command: 'upload-documents',
    }),
  },
  'sign-in': {
    // save: requestWorkflowExecution.bind(undefined, {
    //   stage: 'authorization',
    //   fact: 'identity',
    //   action: 'login-with-otp',
    // }
    save: uplink.requestUplinkExecution.bind(undefined, {
      dock: 'authorization',
      section: 'identity',
      command: 'login-with-otp',
    }),
  },
  'forgot-password': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      dock: 'authorization',
      section: 'identity',
      command: 'password-reset-init',
    }),
  },
  'reset-account': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      dock: 'authorization',
      section: 'identity',
      command: 'reset-account',
    }),
  },
  'verify-email': {
    save: uplink.requestUplinkExecution.bind(undefined, {
      dock: 'authorization',
      section: 'identity',
      command: 'verify-email',
    }),
  },
};


function* save(action) {
  const fail = (response) => put({
    type: SAVE_LOANSTEP_EXECUTION_DENIED,
    meta: {
      ...workflowAction,
      tag: form,
      response,
    },
  });

  const { form, values } = action.meta;
  // If no form provided, fail.
  if (!form) {
    yield fail({
      message: 'No form provided.',
    });
    return;
  }

  // If configuration not found for given form, fail.
  const workflowConfig = workflowMap[form];
  if (!workflowConfig) {
    yield fail({
      message: `Uplink configuration not found for ${form}`,
    });
    return;
  }
  // Load save action. May want to make this generic.
  const workflowAction = workflowMap[form].save;

  // Ask workflow to execute the request configuration.
  const workflowExecutionAccepted = yield workflowAction({ payload: values, tag: form });
  if (!workflowExecutionAccepted) {
    yield fail({ message: 'Uplink did not accept execution request.' });
    return;
  }

  // Ack. the request. (Request accepted and execution started)
  yield put({
    type: `${SAVE_LOANSTEP_STARTED}_${form}`,
    meta: action.meta,
  });

  // Wait for workflow to either fail or succeed.
  const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

  // If workflow executed successfully.
  if (valid) {
    // Yield the result by raising Fulfilled action.
    yield put({
      type: `${SAVE_LOANSTEP_FULFILLED}_${form}`,
      meta: {
        ...workflowAction,
        tag: form,
        response: valid,
      },
    });
  // If execution failed
  } else if (invalid) {
   // Yield error response by raising Failed action.
    if (invalid.meta && invalid.meta.error && invalid.meta.error.body && invalid.meta.error.body.details) {
      const payloadToGenerate = {
        type: '@@redux-form/UPDATE_SYNC_ERRORS',
        meta: {
          form,
        },
        payload: {
          syncErrors: invalid.meta.error.body.details.reduce((a, c) => ({ ...a, ...{ [c.path]: c.message } }), {}),
        },
      };

      yield put(payloadToGenerate);
    }


    yield put({
      type: `${SAVE_LOANSTEP_FAILED}_${form}`,
      meta: {
        ...workflowAction,
        tag: form,
        response: invalid,
      },
    });
  }

  //  Publish ENDED action to signal end of execution.
  yield put({
    type: `${SAVE_LOANSTEP_ENDED}_${form}`,
    meta: {
      ...workflowAction,
      tag: form,
      payload: values,
    },
  });
}


// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery(SAVE_LOANSTEP_REQUEST, save);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
