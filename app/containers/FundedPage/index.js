/*
 *
 * FundedPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { injectIntl, intlShape } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import makeSelectFundedPage from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { CheckPassIcon, HourglassIcon } from '../../components/Icons';
import { appFundedRequest as appFundedRequestActionCreator } from './actions';

const CongratulationText = styled.p`
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

const Heading = styled.p`
    text-align: center;
    font-weight: 600;
    line-height: 16px;
    text-transform: uppercase;
    font-size: 11px;
    letter-spacing: 1px;
`;

const AmoutAndEmi = styled.p`
    text-align: center;
    font-size: 14px;
    letter-spacing: 0.025em;
    font-weight: 100;
`;

export class FundedPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { FundedPage: FundedPageState } = this.props;
    const { applicationFunded } = FundedPageState;

    if (!applicationFunded) {
      this.props.appFundedAction();
    }
  }
  render() {
    const { FundedPage: FundedPageState } = this.props;
    const { applicationFunded, fundedData } = FundedPageState;

    if (!applicationFunded) {
      return (<LoanStep name={'funded'} title={messages.loadingHeader} navigationPanel={() => <span />}>
        <HourglassIcon style={{ transform: 'scale(1.5)' }} />
        <div className="spinner">
          <div className="bounce1" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce2" style={{ backgroundColor: '#79cdd5' }}></div>
          <div className="bounce3" style={{ backgroundColor: '#79cdd5' }}></div>
        </div>
        <CongratulationText>
          Checking if application is funded.
        </CongratulationText>
      </LoanStep>);
    }

    if (!fundedData) {
      return <h1></h1>;
    }

    const { EmiAmount, FundedAmount } = fundedData;

    const formatNumber = (text) => this.props.intl.formatNumber(text, {
      style: 'currency',
      currency: 'INR',
      minimumFractionDigits: 0,
    });

    return (
      <LoanStep navigationPanel={() => <span />} title={messages.header}>
        <CheckPassIcon />
        <CongratulationText />
        <CongratulationText>
          Your loan is approved as follows:
        </CongratulationText>

        <Heading>
          Funded Amount:
        </Heading>
        <AmoutAndEmi>
          {formatNumber(FundedAmount)}
        </AmoutAndEmi>

        <Heading>
          EMI:
        </Heading>
        <AmoutAndEmi>
          {formatNumber(EmiAmount)}
        </AmoutAndEmi>
      </LoanStep>
    );
  }
}

FundedPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  FundedPage: PropTypes.object,
  appFundedAction: PropTypes.func,
  intl: intlShape.isRequired,
};

const mapStateToProps = createStructuredSelector({
  FundedPage: makeSelectFundedPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    appFundedAction: () => dispatch(appFundedRequestActionCreator()),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(injectIntl(FundedPage));
