
import { fromJS } from 'immutable';
import fundedPageReducer from '../reducer';

describe('fundedPageReducer', () => {
  it('returns the initial state', () => {
    expect(fundedPageReducer(undefined, {})).toEqual(fromJS({
      applicationFunded: false,
    }));
  });
});
