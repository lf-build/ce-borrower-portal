import { createSelector } from 'reselect';

/**
 * Direct selector to the fundedPage state domain
 */
const selectFundedPageDomain = () => (state) => state.get('fundedPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FundedPage
 */

const makeSelectFundedPage = () => createSelector(
  selectFundedPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectFundedPage;
export {
  selectFundedPageDomain,
};
