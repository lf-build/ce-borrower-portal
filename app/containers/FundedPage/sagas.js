import { takeEvery, put } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { APP_FUNDED_REQUEST } from './constants';
import * as appFundedActions from './actions';

function* appFunded() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'funded',
  }, {
    tag: 'appFunded',
  }))) {
    return;
  }

  yield put(appFundedActions.appFundedRequestStarted());
  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('appFunded');

  if (valid) {
    yield put(appFundedActions.appFundedRequestFulfilled(valid.meta.body));
  } else {
    yield put(appFundedActions.appFundedRequestFailed(valid.meta.body));
  }

  yield put(appFundedActions.appFundedRequestEnded());
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery(APP_FUNDED_REQUEST, appFunded);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
