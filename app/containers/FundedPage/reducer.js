/*
 *
 * FundedPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  APP_FUNDED_REQUEST_FULFILLED,
  APP_FUNDED_REQUEST_FAILED,
} from './constants';

const initialState = fromJS({
  applicationFunded: false,
});

function fundedPageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case APP_FUNDED_REQUEST_FULFILLED:
      return state
      .set('error', undefined)
      .set('applicationFunded', true)
      .set('fundedData', action.meta.data);
    case APP_FUNDED_REQUEST_FAILED:
      return state
      .set('error', action.meta.error)
      .set('applicationFunded', true);
    default:
      return state;
  }
}

export default fundedPageReducer;
