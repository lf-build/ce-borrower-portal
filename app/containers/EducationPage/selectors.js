import { createSelector } from 'reselect';

/**
 * Direct selector to the educationPage state domain
 */
const selectEducationPageDomain = () => (state) => state.get('educationPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by EducationPage
 */

const makeSelectEducationPage = () => createSelector(
  selectEducationPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectEducationPage;
export {
  selectEducationPageDomain,
};
