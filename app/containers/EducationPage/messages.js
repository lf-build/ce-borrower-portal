/*
 * EducationPage Messages
 *
 * This contains all the text for the EducationPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.EducationPage.header',
    defaultMessage: 'Please complete the verification process to finalise your interest rate',
  },
});
