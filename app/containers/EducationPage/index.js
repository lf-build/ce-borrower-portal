/*
 *
 * EducationPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { Field } from 'redux-form/immutable';
import SelectField from 'redux-form-material-ui/lib/SelectField';
import MenuItem from 'material-ui/MenuItem';
import { createStructuredSelector } from 'reselect';

import makeSelectEducationPage from './selectors';
import messages from './messages';
import TextField from '../../components/RequiredTextField';
import LoanStep from '../../components/LoanStepForm';
import VerificationProcess from '../../components/VerificationProcess';

// validation functions
const required = (value) => value == null ? 'Please tell us your Highest Level Of Education' : undefined;

export class EducationPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  state = {
    value: 0,
  };

  render() {
    const educationLookup = [
      { Code: 'highschool', Value: 'High School' },
      { Code: 'bachelordegree', Value: 'Bachelor Degree' },
      { Code: 'masterdegree', Value: 'Master Degree' },
      { Code: 'doctratedegree', Value: 'Doctrate Degree' }];
    return (
      <LoanStep noBack name={'education'} title={messages.header} noPadding>
        <VerificationProcess step={2} />
        <div style={{ padding: 25 }}>
          <Field
            name="heighestEducation"
            fullWidth
            floatingLabelText={'Highest Level Of Education'}
            floatingLabelStyle={{ fontSize: '12px' }}
            floatingLabelFocusStyle={{ fontSize: '14px' }}
            floatingLabelShrinkStyle={{ fontSize: '14px' }}
            component={SelectField}
            hintText="Highest Level Of Education"
            validate={required}
            value={this.state.value}
            selectedMenuItemStyle={{ color: '#79cdd5' }}
          >
            {educationLookup.map((lookupItem) => <MenuItem key={lookupItem.Code} value={lookupItem.Code} primaryText={lookupItem.Value} />)}
          </Field>


          <TextField
            name="educationalInstitution"
            fullWidth
            hintText="Last Educational Institution"
            floatingLabelText="Last Educational Institution"
          />
        </div>
      </LoanStep>
    );
  }
}

EducationPage.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  EducationPage: makeSelectEducationPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EducationPage);
