import { createSelector } from 'reselect';

/**
 * Direct selector to the mobilePage state domain
 */
const selectMobilePageDomain = () => (state) => state.get('mobilePage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by MobilePage
 */

const makeSelectMobilePage = () => createSelector(
  selectMobilePageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectMobilePage;
export {
  selectMobilePageDomain,
};
