/* eslint-disable react/jsx-indent-props */
/*
 *
 * MobilePage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';
import makeSelectMobilePage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import { MobileIcon } from '../../components/Icons';
// import MaskedTextField from '../../components/MaskedTextField';
import { mobileMask, normalizeMobile } from '../../components/MaskedTextField/masks';
import HintText from '../../components/HintText';

const shortDescription = () => <p> Please enter your mobile number. A One Time Password will be sent to your mobile. </p>;
const Link = styled.div`
  color: #79cdd5;
  cursor: pointer;
  text-align: center;
  margin-bottom:10px;
`;
export class MobilePage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const maskNumber = ({ mobileNumber }) => mobileNumber ? { mobileNumber: normalizeMobile(mobileNumber) } : {};
    const { MobilePage: { loginbtnViz } } = this.props;
    return (
      <LoanStep
        payloadGenerator={maskNumber}
        saveErrorClass={'mobile'}
        name={'mobile'}
        title={messages.header}
        ShortDescription={shortDescription}
        nextButtonName={'Send OTP'}
      >
        <MobileIcon />

        <div className="row">
          <div className={'mobile-label-div'}>
            <label
              htmlFor="mobileNumber"
              className="prefix"
              style={{ paddingRight: '5px' }}
            >+91</label>
          </div>
          <div className={'mobile-textfield-div'}>

            <TextField
              name="mobileNumber"
              fullWidth
              hintText=""
              floatingLabelText="Mobile Number"
              extraValidators={[(value) => value && normalizeMobile(value.toString()).match(/^[6-9][0-9]{9}$/) ? undefined : 'Mobile Number is invalid']}
              mask={mobileMask}
              hintStyle={{ fontWeight: 500, color: 'rgba(0, 0, 0, 0.870588)' }}
              type={'text'}
            />
            <center><RaisedButton style={{ cursor: 'pointer', textAlign: 'center', marginBottom: '10px', color: '#fafafa' }} backgroundColor={'#3663ad'} labelColor={'#fafafa'} className={loginbtnViz ? '' : 'hidden'} onClick={() => browserHistory.replace('/user/sign-in')}>Login</RaisedButton></center>
          </div>
        </div>

        {/* mask="9999-999-999"*/}

        <HintText message={'I authorize Qbera and / or its affiliates to contact me. This will override registry on DND / NDNC.'} />
      </LoanStep >
    );
  }
}

MobilePage.propTypes = {
  MobilePage: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  MobilePage: makeSelectMobilePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MobilePage);
