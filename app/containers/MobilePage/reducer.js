/*
 *
 * MobilePage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  SHOW_LOGIN_BTN,
} from './constants';

const initialState = fromJS({});

function mobilePageReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    case SHOW_LOGIN_BTN:
      return state.set('loginbtnViz', action.loginbtnViz);
    default:
      return state;
  }
}

export default mobilePageReducer;
