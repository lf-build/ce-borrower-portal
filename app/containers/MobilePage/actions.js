/*
 *
 * MobilePage actions
 *
 */

import {
  DEFAULT_ACTION,
  SHOW_LOGIN_BTN,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function showLoginButton(loginbtnViz) {
  return {
    type: SHOW_LOGIN_BTN,
    loginbtnViz,
  };
}
