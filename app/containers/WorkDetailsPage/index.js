/*
 *
 * WorkDetailsPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
// import { createStructuredSelector } from 'reselect';
import { Field, formValueSelector } from 'redux-form/immutable';
// import makeSelectWorkDetailsPage from './selectors';
import messages from './messages';
import { requestLookup as requestLookupActionCreator } from '../../sagas/lookup/actions';
import AutoComplete from '../../components/RemoteAutoComplete';

import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import HintText from '../../components/HintText';
import { moneyMask, normalizeMoney } from '../../components/MaskedTextField/masks';
// import { emailMask, moneyMask, normalizeMoney } from '../../components/MaskedTextField/masks';
import makeAppSelect from '../App/selectors';
import {
  employerNameChange as employerNameChangeActionCreator,
  saveEmpNameFreeText as saveEmpNameFreeTextActionCreator,
 } from './actions';

const requiredValidation = (value) => {
  if (!value) {
    return 'Please select an Employer Name';
  }
  return undefined;
};

const WorkDetailsPageFormSelector = formValueSelector('work-details');

export class WorkDetailsPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { employer, setEmployerFromState } = this.props;
    if (employer) {
      const emp = employer.toJS ? employer.toJS() : employer;
      if (emp) {
        setEmployerFromState(emp);
      }
    }
  }

  render() {
    // const {
    //   WorkDetailsPage: { employers },
    // } = this.props;
    const dataSourceConfig = {
      text: 'name',
      value: 'cin',
    };
    const normalizeMonthlyTakeHomeSalary = ({ monthlyTakeHomeSalary }) => ({
      monthlyTakeHomeSalary: normalizeMoney(monthlyTakeHomeSalary),
    });
    const { reviewPage, employerNameChange, saveEmployerFreeText } = this.props;

    const isIOS = (/iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream);
    const employerPopoverProps = {
      // canAutoPosition: true,
      // anchorOrigin: { horizontal: 'left', vertical: 'bottom' },
      // targetOrigin: { horizontal: 'left', vertical: 'top' },
      ...(isIOS ? {
        style: {
          position: 'absolute',
        },
      } : {}),
    };

    return (
      <LoanStep
        simulateCurrentRoute={reviewPage}
        nextButtonName={reviewPage && 'Go to review'}
        noBack
        payloadGenerator={normalizeMonthlyTakeHomeSalary}
        name={'work-details'}
        title={messages.header}
      >
        <Field
          validate={requiredValidation}
          name="employer"
          component={AutoComplete}
          floatingLabelText="Employer Name (Legal Business Name)"
          floatingLabelStyle={{ fontSize: 12 }}
          floatingLabelFocusStyle={{ fontSize: '14px', width: '262px' }}
          floatingLabelShrinkStyle={{ fontSize: '14px', width: '262px' }}
          fullWidth
          openOnFocus
          dataSourceConfig={dataSourceConfig}
          listStyle={{ maxHeight: '200px', overflowY: 'auto' }}
          empNameChange={employerNameChange}
          onBlurEvent={saveEmployerFreeText}
          popoverProps={employerPopoverProps}
        />
        {/* dataSource={employers}*/}
        <HintText message={'Showing Results 20 of many. Please continue typing until you see your company name and select it from the list.'} />

        <div className="row">
          <div className={'amount-label-div'}>
            <label
              htmlFor="monthlyTakeHomeSalary"
              className="prefix"
              style={{ paddingRight: '4px' }}
            >₹</label>
          </div>
          <div className={'amount-textfield-div'}>
            <TextField
              name="monthlyTakeHomeSalary"
              fullWidth
              hintText=""
              mask={moneyMask}
              floatingLabelText="Monthly Net Take Home Salary"
              extraValidators={[(sal) => {
                if (/[^\d]/.test(sal)) {
                  return 'Please enter a valid number';
                }
                if (sal.length > 6) {
                  return 'Entered amount is out of range';
                }
                const salary = parseInt(sal.toString(), 10) || 0;
                if (salary > 0) {
                  return undefined;
                }
                // return 'Your monthly take home salary must be equal to or higher than ₹20,000';
                return undefined;
              }]}
              floatingLabelFocusStyle={{ width: '321px' }}
              floatingLabelShrinkStyle={{ width: '321px' }}
            />
            <HintText message={'This salary amount should reflect in your salary account bank statement.'} />
          </div>

        </div>

        <TextField
          name="officialEmail"
          fullWidth
          hintText="Official E-mail ID"
          floatingLabelText="Official E-mail ID"
          email
        />
        {/* mask={emailMask} */}
      </LoanStep>
    );
  }
}

WorkDetailsPage.propTypes = {
  // WorkDetailsPage: React.PropTypes.object,
  // App: React.PropTypes.object,
  setEmployerFromState: React.PropTypes.func,
  reviewPage: React.PropTypes.string,
  employer: React.PropTypes.object,
  employerNameChange: React.PropTypes.func,
  saveEmployerFreeText: React.PropTypes.func,
};

// const mapStateToProps = createStructuredSelector({
//   WorkDetailsPage: makeSelectWorkDetailsPage(),
//   App: makeAppSelect(),
//   // form: getFormValues('work-details'),
// });

const mapStateToProps = (state) => ({
  ...({ employer: WorkDetailsPageFormSelector(state, 'employer') }),
  ...makeAppSelect()(state),
});

function mapDispatchToProps(dispatch) {
  return {
    requestLookup: (searchText) => dispatch(requestLookupActionCreator({
      entity: 'company',
      tag: (new Date()).getTime().toString(),
      searchText,
    })),
    setEmployerFromState: (employer) => dispatch({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'work-details',
        field: 'employer',
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: {
        cin: employer.cin,
        name: employer.name,
      },
    }),
    employerNameChange: (text) => dispatch(employerNameChangeActionCreator(text)),
    saveEmployerFreeText: (text) => dispatch(saveEmpNameFreeTextActionCreator(text)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WorkDetailsPage);
