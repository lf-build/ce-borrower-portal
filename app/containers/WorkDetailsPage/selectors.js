import { createSelector } from 'reselect';

/**
 * Direct selector to the workDetailsPage state domain
 */
const selectWorkDetailsPageDomain = () => (state) => state.get('workDetailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by WorkDetailsPage
 */

const makeSelectWorkDetailsPage = () => createSelector(
  selectWorkDetailsPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectWorkDetailsPage;
export {
  selectWorkDetailsPageDomain,
};
