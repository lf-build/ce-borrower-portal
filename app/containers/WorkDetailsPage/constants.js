/*
 *
 * WorkDetailsPage constants
 *
 */

export const DEFAULT_ACTION = 'app/WorkDetailsPage/DEFAULT_ACTION';
export const EMPLOYER_NAME_CHANGE = 'app/WorkDetailsPage/EMPLOYER_NAME_CHANGE';
export const SAVE_EMP_NAME_FREE_TEXT = 'app/WorkDetailsPage/SAVE_EMP_NAME_FREE_TEXT';
