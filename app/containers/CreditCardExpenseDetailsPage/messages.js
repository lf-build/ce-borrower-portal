/*
 * CreditCardExpenseDetailsPage Messages
 *
 * This contains all the text for the CreditCardExpenseDetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.CreditCardExpenseDetailsPage.header',
    defaultMessage: 'A little about expenses',
  },
});
