import { createSelector } from 'reselect';

/**
 * Direct selector to the creditCardExpenseDetailsPage state domain
 */
const selectCreditCardExpenseDetailsPageDomain = () => (state) => state.get('creditCardExpenseDetailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by CreditCardExpenseDetailsPage
 */

const makeSelectCreditCardExpenseDetailsPage = () => createSelector(
  selectCreditCardExpenseDetailsPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectCreditCardExpenseDetailsPage;
export {
  selectCreditCardExpenseDetailsPageDomain,
};
