import { createSelector } from 'reselect';

/**
 * Direct selector to the basicInformationPage state domain
 */
const selectBasicInformationPageDomain = () => (state) => state.get('basicInformationPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by BasicInformationPage
 */

const makeSelectBasicInformationPage = () => createSelector(
  selectBasicInformationPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectBasicInformationPage;
export {
  selectBasicInformationPageDomain,
};
