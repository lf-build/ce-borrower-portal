/**
 *
 * App.react.js
 *
 * This component is the skeleton around the actual pages, and should only
 * contain code that should be seen on all pages. (e.g. navigation bar)
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a necessity for you then you can refactor it and remove
 * the linting exception.
 */

import React from 'react';
// import { RouteTransition, presets } from 'react-router-transition';
import { presets } from 'react-router-transition';
// import LinearProgress from 'material-ui/LinearProgress';

import { withRouter } from 'react-router';
import Header from '../../components/Header';
import LeftSidebar from '../LeftSidebar';
import RightSidebar from '../RightSidebar';

import ApplicationProgress from '../ApplicationProgress';

class App extends React.Component { // eslint-disable-line react/prefer-stateless-function

  static propTypes = {
    children: React.PropTypes.node,
    // location: React.PropTypes.object,
  }

  render() {
    const slideRight = presets.slideRight;

    slideRight.mapStyles = (styles) => ({
      opacity: styles.opacity,
      transform: `translateX(${styles.offset}%)`,
    });
    const pathSplit = location.pathname.split('/');
    const page = pathSplit.length >= 3 ? pathSplit[2] : 'none';
    const terminalPages = ['application-submitted',
      'application-under-review',
      'verification-failed',
      // 'application-loan-agreement',
      // 'application-rejected',
      'not-interested',
    ];
    const leftSideVisible = terminalPages.filter((p) => p === page).length === 0;
    return (
      <div>
        <Header />
        <div className="container-fluid main-wrap no-padd">
          <div className="col-md-3 no-padd hidden-sm hidden-xs mob-nav">
            <div className="heading">
              <div className="mobile-nav">

                <ol className="progress">
                  <li className="is-complete" data-step="1"> Loan Details </li>
                  <li className="is-complete" data-step="2"> Personal Information </li>
                  <li className="is-active" data-step="3"> Verification </li>
                  <li data-step="4" className="progress-last"> Loan Offer </li>
                </ol>

              </div>
            </div>
          </div>

          <div className={`col-md-3 no-padd hidden-sm hidden-xs column left ${leftSideVisible ? 'loan-step-background' : ''}`}>
            {leftSideVisible ? <LeftSidebar /> : <span />}
          </div>

          <div className="col-md-6 col-sm-7 col-xs-12 column middle">
            <ApplicationProgress className={`hidden-md hidden-lg ${leftSideVisible ? '' : 'hidden'}`} orientation={'horizontal'} />
            {React.Children.toArray(this.props.children)}
          </div>
          <div className="col-md-3 col-sm-5 col-xs-12 column right faq-privacy-background">
            <RightSidebar page={page} />
          </div>
        </div>

      </div>
    );
  }
}

export default withRouter(App);
