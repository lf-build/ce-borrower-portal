/**
 * Combine all reducers in this file and export the combined reducers.
 * If we were to do this in store.js, reducers wouldn't be hot reloadable.
 */
import { fromJS } from 'immutable';

import applicationProgressReducer from '../ApplicationProgress/reducer';
import navigationHelperReducer from '../NavigationHelper/reducer';
import rightSidebarReducer from '../RightSidebar/reducer';
import loanStepReducer from '../LoanStep/reducer';
import authReducer from '../../sagas/auth/reducer';
import reasonPageReducer from '../../containers/ReasonPage/reducer';

import { SUBMIT_APPLICATION_REQUEST_FULFILLED } from '../ReviewApplicationPage/constants';
import {
  GENERATE_INITIAL_OFFER_REQUEST_FULFILLED,
  GET_BASIC_APP_INFO_REQUEST_FULFILLED,
} from '../InitialOfferPage/constants';
import { NOT_INTERESTED_REQUEST_FULFILLED } from '../../components/NotInterested/constants';
import { LOAN_STEP_LOADED } from '../LoanStep/constants';
import {
  UPDATE_STATUS_CALLED_FIRST_TIME,
  SHOW_HIDE_NET_BANKING_LOADER,
} from '../NetLinkEmbeddedForm/constants';
import { SET_DOB_ERROR_MSG, SET_YM_ERROR_MSG } from '../PersonalDetailsPage/constants';
import { GET_INITIAL_OFFER_INFO_REQUEST_FULFILLED } from '../SocialPage/constants';

const initialState = fromJS({});
function appReducer(state = initialState, action) {
  switch (action.type) {
    case NOT_INTERESTED_REQUEST_FULFILLED:
      return state
        .set('status', 'not-interested');
    case LOAN_STEP_LOADED:
      if (action.meta.name === 'review-application') {
        return state
          .set('reviewPage', 'personalDetailsPage');
      }
      return state;
    case SUBMIT_APPLICATION_REQUEST_FULFILLED:
      return state
        // .set('applicationSubmited', true)
        .set('status', 'submited')
        .set('application', action.meta.body);
    case GENERATE_INITIAL_OFFER_REQUEST_FULFILLED:
      return state
        .set('initialOffer', action.meta.body);
    case UPDATE_STATUS_CALLED_FIRST_TIME:
      return state.set('statusCalledFirstTime', action.payload.status);
    case SHOW_HIDE_NET_BANKING_LOADER:
      return state.set('showNetbankingLoader', action.payload.value);
    case SET_DOB_ERROR_MSG:
      return state.set('errorMsg', action.payload.errorMsg);
    case SET_YM_ERROR_MSG:
      return state.set('errorYMMsg', action.payload.errorMsg);
    case GET_BASIC_APP_INFO_REQUEST_FULFILLED:
      return state
        .set('application', action.meta.appInfo);
    case GET_INITIAL_OFFER_INFO_REQUEST_FULFILLED:
      return state
        .set('initialOffer', action.meta.offerInfo);
    default:
      return state;
  }
}

/**
 * Creates the main reducer with the asynchronously loaded ones
 */
export default {
  app: appReducer,
  applicationProgress: applicationProgressReducer,
  navigationHelper: navigationHelperReducer,
  rightSidebar: rightSidebarReducer,
  loanStep: loanStepReducer,
  auth: authReducer,
  reasonPage: reasonPageReducer,
};
