import { put, takeLatest } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { GET_BASIC_APP_INFO_REQUEST } from '../InitialOfferPage/constants';
import * as actions from '../InitialOfferPage/actions';

export function* getBasicAppInfo() {
  const form = 'get-basic-app-info';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'application',
    command: 'get-basic-app-info',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getBasicAppInfoRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(actions.getBasicAppInfoRequestFulfilled(valid.meta.body));
    } else {
      yield put(actions.getBasicAppInfoRequestFailed('error while fetching basic app information'));
    }

    yield put(actions.getBasicAppInfoRequestEnded());
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeLatest(GET_BASIC_APP_INFO_REQUEST, getBasicAppInfo);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
