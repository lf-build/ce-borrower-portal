import { createSelector } from 'reselect';

/**
 * Direct selector to the applicationRejected state domain
 */
const selectApplicationRejectedDomain = () => (state) => state.get('applicationRejected');

/**
 * Other specific selectors
 */


/**
 * Default selector used by ApplicationRejected
 */

const makeSelectApplicationRejected = () => createSelector(
  selectApplicationRejectedDomain(),
  (substate) => substate.toJS()
);

export default makeSelectApplicationRejected;
export {
  selectApplicationRejectedDomain,
};
