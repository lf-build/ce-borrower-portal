/*
 *
 * ApplicationRejected
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { createStructuredSelector } from 'reselect';
import makeSelectApplicationRejected from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { CheckFailIcon } from '../../components/Icons';
import makeSelectApp from '../App/selectors';
import Referral from '../../components/Referral';
import { getBasicAppInfoRequest } from '../InitialOfferPage/actions';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 400;
    margin: 20px 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

export class ApplicationRejected extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { App: { application } } = this.props;
    if (!application) {
      this.props.dispatch(getBasicAppInfoRequest());
    }
  }

  render() {
    const { App: { application } } = this.props;

    if (!application) {
      return <span />;
    }

    const { firstName, mobileNumber } = application;

    return (
      <LoanStep navigationPanel={() => <Referral name={firstName} mobile={mobileNumber} />} title={messages.header}>
        <CheckFailIcon />
        <CongratulationText />
        <CongratulationText>
          Unfortunately, we could not approve your loan application under Qbera's credit policy.
        </CongratulationText>
      </LoanStep>
    );
  }
}

ApplicationRejected.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  App: PropTypes.object,
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ApplicationRejected: makeSelectApplicationRejected(),
  App: makeSelectApp(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ApplicationRejected);
