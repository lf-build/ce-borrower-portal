/*
 *
 * FinalOfferPage actions
 *
 */

import {
  DEFAULT_ACTION,
  OFFER_SELECTED,
  FINAL_OFFER_GENERATE_REQUEST,
  FINAL_OFFER_GENERATE_REQUEST_STARTED,
  FINAL_OFFER_GENERATE_REQUEST_FULFILLED,
  FINAL_OFFER_GENERATE_REQUEST_FAILED,
  FINAL_OFFER_GENERATE_REQUEST_ENDED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}
export function offerSelected(id) {
  return {
    type: OFFER_SELECTED,
    meta: {
      id: parseInt(id, 10),
    },
  };
}
export function generateFinalOfferRequest() {
  return {
    type: FINAL_OFFER_GENERATE_REQUEST,
  };
}

export function generateFinalOfferRequestStarted() {
  return {
    type: FINAL_OFFER_GENERATE_REQUEST_STARTED,
  };
}

export function generateFinalOfferRequestFulfilled(offer) {
  return {
    type: FINAL_OFFER_GENERATE_REQUEST_FULFILLED,
    meta: {
      offer,
    },
  };
}

export function generateFinalOfferRequestFailed(error) {
  return {
    type: FINAL_OFFER_GENERATE_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function generateFinalOfferRequestEnded() {
  return {
    type: FINAL_OFFER_GENERATE_REQUEST_ENDED,
  };
}
