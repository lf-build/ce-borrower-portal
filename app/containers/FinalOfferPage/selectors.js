import { createSelector } from 'reselect';

/**
 * Direct selector to the finalOfferPage state domain
 */
const selectFinalOfferPageDomain = () => (state) => state.get('finalOfferPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by FinalOfferPage
 */

const makeSelectFinalOfferPage = () => createSelector(
  selectFinalOfferPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectFinalOfferPage;
export {
  selectFinalOfferPageDomain,
};
