
import { fromJS } from 'immutable';
import finalOfferPageReducer from '../reducer';

describe('finalOfferPageReducer', () => {
  it('returns the initial state', () => {
    expect(finalOfferPageReducer(undefined, {})).toEqual(fromJS({
      offerPending: true,
    }));
  });
});
