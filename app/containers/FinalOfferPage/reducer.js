/*
 *
 * FinalOfferPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  FINAL_OFFER_GENERATE_REQUEST_FULFILLED,
  FINAL_OFFER_GENERATE_REQUEST_FAILED,
  FINAL_OFFER_GENERATE_REQUEST_ENDED,
  FINAL_OFFER_GENERATE_REQUEST_STARTED,
  OFFER_SELECTED,
} from './constants';

const initialState = fromJS({
  offerPending: true,
});

const extractAndSelectOffer = (offer, selectedIndex) => {
  let extractedOffer = offer;
  if (offer && offer.toJS) {
    extractedOffer = { ...offer.toJS() };
  }
  extractedOffer.finalOffers = selectOffer(extractedOffer.finalOffers, selectedIndex);
  return { ...extractedOffer };
};

const selectOffer = (offers, selectedIndex) => ((offers.toJS && offers.toJS()) || offers).map(
    (offer, index) => ({
      ...offer,
      ...{ isSelected: index === selectedIndex },
    }));
function finalOfferPageReducer(state = initialState, action) {
  switch (action.type) {
    case FINAL_OFFER_GENERATE_REQUEST_STARTED:
      return state
            .set('error', undefined)
            .set('offer', undefined)
            .set('loading', true);
    case FINAL_OFFER_GENERATE_REQUEST_ENDED:
      return state.set('loading', false);
    case FINAL_OFFER_GENERATE_REQUEST_FULFILLED:
      return state
      .set('error', undefined)
      .set('offer', action.meta.offer)
      .set('offerPending', false);
    case FINAL_OFFER_GENERATE_REQUEST_FAILED:
      return state
      .set('error', action.meta.error)
      .set('offerPending', false);
    case OFFER_SELECTED:
      return state
              .set('offer', extractAndSelectOffer(state.get('offer'), action.meta.id));
    default:
      return state;
  }
}

export default finalOfferPageReducer;
