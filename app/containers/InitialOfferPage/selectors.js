import { createSelector } from 'reselect';

/**
 * Direct selector to the initialOfferPage state domain
 */
const selectInitialOfferPageDomain = () => (state) => state.get('initialOfferPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by InitialOfferPage
 */

const makeSelectInitialOfferPage = () => createSelector(
  selectInitialOfferPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectInitialOfferPage;
export {
  selectInitialOfferPageDomain,
};
