import { put, takeLatest } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { navigationHelper } from '../NavigationHelper/helper';


// import { LOCATION_CHANGE } from 'react-router-redux';
import {
  GENERATE_INITIAL_OFFER_REQUEST,
  GET_BASIC_APP_INFO_REQUEST,
} from './constants';
import * as actions from './actions';

export function* generateInitialOffer() {
  // if (action.meta.body.type === 'lead') {
  //   yield navigationHelper.goTo({
  //     name: 'review',
  //     params: {
  //     },
  //   }, 'applicationSubmittedPage');
  //   return;
  // }

  const form = 'review';
  // Select username from store
  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'offer',
    command: 'generate-initial',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.generateInitialOfferStarted());

    // Wait for Uplink to either fail or succeed.
    const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);
    if (valid) {
      if (valid.meta && valid.meta.body && valid.meta.body.status === 'Rejected') {
        navigationHelper.goTo({ name: 'initialOffer' }, 'applicationRejectedPage', false);
      }
      yield put(actions.generateInitialOfferFulfilled(valid.meta));
    } else {
      yield put(actions.generateInitialOfferFailed(invalid.meta));
      navigationHelper.goTo({ name: 'initialOffer' }, 'cibilFailed', false);
    }
  } else {
    yield put(actions.generateInitialOfferDenied({ message: 'Uplink did not accept the request.' }));
    navigationHelper.goTo({ name: 'initialOffer' }, 'cibilFailed', false);
  }

  yield put(actions.generateInitialOfferEnded());
}

export function* getBasicAppInfo() {
  const form = 'get-basic-app-info';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'application',
    command: 'get-basic-app-info',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getBasicAppInfoRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(actions.getBasicAppInfoRequestFulfilled(valid.meta.body));
    } else {
      yield put(actions.getBasicAppInfoRequestFailed('error while fetching basic app information'));
    }

    yield put(actions.getBasicAppInfoRequestEnded());
  }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeLatest(GENERATE_INITIAL_OFFER_REQUEST, generateInitialOffer);
  yield takeLatest(GET_BASIC_APP_INFO_REQUEST, getBasicAppInfo);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
