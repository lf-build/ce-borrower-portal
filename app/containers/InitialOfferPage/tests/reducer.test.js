
import { fromJS } from 'immutable';
import initialOfferPageReducer from '../reducer';

describe('initialOfferPageReducer', () => {
  it('returns the initial state', () => {
    expect(initialOfferPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
