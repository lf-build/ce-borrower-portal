/*
 *
 * BankingPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // DEFAULT_ACTION,
  STATEMENT_RECIEVED,
  STATEMENT_REMOVED,
  ERROR_OCCURRED,
  SET_NET_LINK_TRANSACTION,
  NET_LINK_TRANSACTION_FAILED,
} from './constants';

const initialState = fromJS({
  statements: [],
  errorMsg: undefined,
  isNetlinkingDone: false,
});

function bankingPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'tab-selected':
      return state.set('selectedTab', action.id);
    case '@@redux-form/CHANGE':
      if (action.meta.form === 'banking' && action.meta.field === 'bankAccountConnect') {
        return state
        .set('bankAccountConnect', action.payload);
      }
      return state;
    case STATEMENT_RECIEVED:
      return state.set('statements', state.get('statements').push(action.payload));
    case STATEMENT_REMOVED:
      return state.set('statements', state.get('statements').filter((item, index) => index !== action.payload.index));
    case ERROR_OCCURRED:
      return state.set('errorMsg', action.payload.errorMsg);
    case SET_NET_LINK_TRANSACTION:
      return state.set('isNetlinkingDone', action.payload.status)
                  .set('showLoader', false);
    case 'show-loader':
      return state.set('showLoader', action.value);
    case NET_LINK_TRANSACTION_FAILED:
      return state.set('showLoader', action.payload.value);
    default:
      return state;
  }
}

export default bankingPageReducer;
