/*
 *
 * BankingPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import MUIAutoComplete from 'material-ui/AutoComplete';
import { RadioButton, RadioButtonGroup } from 'material-ui/RadioButton';
import RaisedButton from 'material-ui/RaisedButton';
import { Field } from 'redux-form/immutable';
// import { RadioButton } from 'material-ui/RadioButton';
// import CheckBoxOn from 'material-ui/svg-icons/toggle/check-box';
// import CheckBoxOff from 'material-ui/svg-icons/toggle/check-box-outline-blank';
import { createStructuredSelector } from 'reselect';
import messages from './messages';
import {
  statementRecieved as statementRecievedActionCreator,
  statementRemoved as statementRemovedActionCreator,
  bankNameChange as bankNameChangeActionCreator,
} from './actions';
import TextField from '../../components/RequiredTextField';
import LoanStep from '../../components/LoanStepForm';
import VerificationProcess from '../../components/VerificationProcess';
import makeSelectBankingPage from './selectors';
import FileDropZone from '../../components/FileDropZone';
import banklookup from './banklookup.json';
import AutoComplete from '../../components/AutoComplete'; // 'redux-form-material-ui/lib/AutoComplete';
import HintText from '../../components/HintText';
import { navigationHelper } from '../NavigationHelper/helper';
import DefaultLoanStepNavigationPanel from '../../components/DefaultLoanStepNavigationPanel';
import {
  updateStatusCalledFirstTime as updateStatusCalledFirstTimeActionCreator,
  netBankingLoader as netBankingLoaderActionCreator,
} from '../NetLinkEmbeddedForm/actions';

const BankingInfo = styled.p`
    // color: #c9c9c9;
    // font-size: 11px;
    // font-weight: 400;
    // letter-spacing: 0.025em;
    // margin: 10px 0 20px 0;
    // line-height: normal;
    font-size: 12px;
    color: #000;
    line-height: 22px;
    text-align: left;
`;

const styles = {
  activeOption: {
    background: 'rgba(44, 198, 199, 0.08) none repeat scroll 0 0',
    border: '1px solid #2cc6c7',
    boxShadow: 0,
    color: '#666',
    borderRadius: 5,
  },
  errorMsgStyle: {
    position: 'relative',
    bottom: '5px',
    fontSize: '12px',
    lineHeight: '15px',
    color: 'rgb(255, 117, 106)',
    transition: 'all 450ms cubic-bezier(0.23, 1, 0.32, 1) 0ms',
    marginTop: '16px',
  },
};

const requiredValidation = (value) => {
  if (!value) {
    return 'Bank Name cannot be left blank';
  }
  return undefined;
};

export class BankingPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    let { BankingPageState } = this.props;
    const {
      statementRecieved,
      statementRemoved,
      selectTab,
      BankingPageState: { selectedTab = 'connect', showLoader = false },
      bankNameChng,
      setLoader,
      updateStatusCalled,
      setNetBankingLoader } = this.props;

    const isIOS = (/iPad|iPhone|iPod/.test(window.navigator.userAgent) && !window.MSStream);
    // const isMobileDevice = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
    const hideStatementUpload = process.env.PERFIOS_LIVE && process.env.PERFIOS_LIVE === 'LIVE';

    if (!BankingPageState) {
      BankingPageState = {};
    }
    const { statements, errorMsg, isNetlinkingDone } = BankingPageState;
    const banks = banklookup.instiutions;
    const dataSourceConfig = {
      text: 'name',
      value: 'id',
    };
    const buildPayload = () => ({
      statements,
    });
    const handleStatementDropped = (file) => {
      statementRecieved(file);
    };
    const handleStatementRemoved = (file, index) => {
      statementRemoved(index);
    };
    const handleTabChange = (e) => {
      if (e.target.value !== 'connect') {
        setLoader(false);
      }
      selectTab(e.target.value);
    };
    const handleBankConnect = () => {
      // here this call initiate the net bank linking.
      // here netlink form will be fetched only if transaction is not completed.
      setLoader(true);
      // On every click of Continue we will set the statusCalledFirstTime parameter as true, so Salary bank account can be connected.
      updateStatusCalled(true);
      setNetBankingLoader(true);
      if (!isNetlinkingDone) {
        setTimeout(() => navigationHelper.goTo({ name: 'banking' }, 'netlinkPage'), 100);
      }
    };

    if (selectedTab === 'connect') {
      // the below code is for yodlee
      // setTimeout(() => {
      //   if (document.getElementById('netlinkEmbeded')) {
      //     document.getElementById('netlinkEmbeded').style.width = '100%';
      //     document.getElementById('netlinkEmbeded').style.height = '350px';
      //   }
      //   if (document.getElementById('yodleeLoader')) {
      //     document.getElementById('yodleeLoader').remove();
      //   }
      // }, 10000);
    }

    const bankPopoverProps = {
      // canAutoPosition: true,
      anchorOrigin: { horizontal: 'left', vertical: 'bottom' },
      targetOrigin: { horizontal: 'left', vertical: 'top' },
      ...(isIOS ? {
        style: {
          position: 'absolute',
        },
      } : {}),
    };
    return (
      <LoanStep
        noBack
        payloadGenerator={buildPayload}
        name={'banking'}
        title={messages.header}
        noPadding
        disableNext={statements && statements.length === 0}
        navigationPanel={(props) => selectedTab === 'connect' ? <span /> : <DefaultLoanStepNavigationPanel {...props} />}
      >
        <VerificationProcess step={0} />

        <div style={{ padding: 25 }}>
          <div className={'manual-label'}>256-Bit Encryption</div>
          <br />

          <ul className="verificationOption">
            <RadioButtonGroup valueSelected={selectedTab} name="verificationMethod" defaultSelected="upload" onChange={handleTabChange}>
              <RadioButton
                value="connect"
                label={<li style={{ listStyle: 'none' }}>
                  <span><strong style={{ color: '#666', fontSize: 14 }}>Verify your salary credits via Netbanking and get your loan offer</strong></span><br />
                  <span style={{ fontSize: '10px', lineHeight: '16px' }}>Submit your salary bank account statement directly from your bank</span>
                </li>}
                iconStyle={{ marginTop: 7, marginLeft: 5 }}
                style={selectedTab === 'connect' ? styles.activeOption : {}}
              />
              <RadioButton
                value="upload"
                label={<li style={{ listStyle: 'none' }} >
                  <span style={{ color: '#666', fontSize: 14 }}><strong>Upload salary bank account statement</strong></span><span style={{ fontSize: 12, color: '#c9c9c9' }}> (PDF)</span><br />
                  <span style={{ fontSize: '10px', lineHeight: '16px' }}>Please upload last 3 months (90 days’) bank-generated PDF bank statement(s) from your salary account, so that we can quickly calculate your final offer. The statement should be updated till today.</span>
                </li>}
                style={selectedTab === 'upload' ? styles.activeOption : {}}
                iconStyle={{ marginTop: 7, marginLeft: 5 }}
                className={hideStatementUpload ? 'hidden' : ''}
              />
            </RadioButtonGroup>
          </ul>
          {
            selectedTab === 'connect' ?
              (
                <div>
                  <div style={{ minHeight: '50px', display: `${showLoader ? 'block' : 'none'}` }}>
                    <div id="yodleeLoader" className={'col-sm-12'} style={{ textAlign: 'center', height: '50px', float: 'left' }}>
                      <span className={'col-sm-3'} />
                      <div id="lenddo_verify">
                        <div id="lenddoSpinner" className="spinner">
                          <div className="bounce1"></div>
                          <div className="bounce2"></div>
                          <div className="bounce3"></div>
                        </div>
                      </div>
                      <span className={'col-sm-3'} />
                    </div>
                    {/* <embed id="yodleeEmbeded" style={{ width: '0px', height: '0px' }} src="http://localhost:3000/user/yodlee" />*/}
                  </div>
                  <div className={`${isNetlinkingDone ? 'hidden' : ''}`}>
                    <div style={{ textAlign: 'center', marginTop: '15px', marginBottom: '15px' }}>
                      <RaisedButton
                        label={'Continue'}
                        onTouchTap={handleBankConnect}
                        labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
                        backgroundColor="#3663ad"
                        labelColor={'#ffffff'}
                      />
                    </div>
                    <BankingInfo>
                      {/* You will be redirected to the net banking connect gateway and you will be brought back to the application form upon successful connect. */}
                      Upon clicking Continue you will be taken to a secure gateway operated by Perfios and you will be brought back automatically to this application form upon successful pulling of your bank statement. Please do not close the window or click Cancel during the process which may take a few minutes at most.
                      <br /><br />
                      Perfios is our authorised service provider for fetching original bank statements directly from your bank account and the credentials entered by you are solely used for this purpose and nothing else. Your passwords are NEVER stored. Perfios is an ISO 27001 certified company and it also has PLYNT certification which is one of the best financial application security standard in the world. Perfios is also TrustGuard certified which means it performs more than 40000 penetration testing each day ensuring the safety and security of the application. All the leading Insurance companies, Banks, NBFCs and Mutual Fund companies in India use Perfios.
                    </BankingInfo>
                  </div>
                  <div className={`${isNetlinkingDone ? '' : 'hidden'}`}>
                    <BankingInfo>Salary Bank Account connected successfully.</BankingInfo>
                  </div>
                </div>)
              :
              (<div id="perfios" className={selectedTab === undefined || selectedTab === 'connect' || hideStatementUpload ? 'hidden' : ''}>
                <BankingInfo style={{ marginTop: '10px' }}>
                  Click <a target="_blank" href="http://help.qbera.com/where-do-i-find-my-bank-statement" style={{ color: '#79cdd5', cursor: 'pointer', textDecoration: 'none' }}>here</a> for instructions on how to download your bank statement(s).
                  <ul style={{ marginTop: '10px', marginLeft: '20px', fontSize: '11px', lineHeight: '18px' }}>
                    <li>Please ensure your bank statement(s) is / are in bank-generated, PDF format (pictures, scans and edited files can NOT be processed). You may upload multiple files.</li>
                    <li>Do not tamper with the file in any way, including converting files or merging them.</li>
                  </ul>
                </BankingInfo>

                <Field
                  validate={requiredValidation}
                  name="bank"
                  component={AutoComplete}
                  floatingLabelStyle={{ fontSize: 14 }}
                  floatingLabelText="Salary Bank Account"
                  fullWidth
                  openOnFocus
                  onUpdateInput={(text) => bankNameChng(text)}
                  filter={MUIAutoComplete.fuzzyFilter}
                  onNewRequest={(value) => {
                    console.log('AutoComplete ', value); // eslint-disable-line no-console
                  }}
                  dataSourceConfig={dataSourceConfig}
                  dataSource={banks}
                  listStyle={{ maxHeight: '200px', overflowY: 'auto' }}
                  // anchorOrigin={{ vertical: 'top', horizontal: 'left' }}
                  // targetOrigin={{ vertical: 'bottom', horizontal: 'left' }}
                  popoverProps={bankPopoverProps}
                />
                <HintText message={'Please type your bank name slowly. If you are unable to see your bank name then please contact us and we will have your bank statement processed manually.'} />

                {/* <TextField
                  id="accountNumber"
                  name="accountNumber"
                  fullWidth
                  hintText="Account Number"
                  floatingLabelText="Account Number"
                  type="password"
                  minLen={5}
                  maxLen={30}
                />
                <TextField
                  id="confirmAccountNumber"
                  name="confirmAccountNumber"
                  fullWidth
                  hintText="Confirm Account Number"
                  floatingLabelText="Confirm Account Number"
                  type="password"
                  minLen={5}
                  maxLen={30}
                  extraValidators={[() => document.getElementById('accountNumber').value === document.getElementById('confirmAccountNumber').value ? undefined : 'Both account numbers must match']}
                /> */}
                {/* <TextField
                  name="ifscCode"
                  fullWidth
                  hintText="IFSC Code"
                  floatingLabelText="IFSC Code"
                  minLen={11}
                  maxLen={11}
                />
                <TextField
                  name="bankAddress"
                  fullWidth
                  hintText="Please enter IFSC Code above to auto populate bank address."
                  hintStyle={{ fontSize: 12 }}
                  floatingLabelText="Bank Address"
                  minLen={2}
                  maxLen={300}
                  multiLine
                  readOnly
                /> */}
                <br />
                <div style={{ marginBottom: '0px' }} className={'manual-label'}>Bank Statement File Upload</div>
                <div className={'file-upload-block'}>
                  <FileDropZone
                    allowMultiple
                    onDrop={handleStatementDropped}
                    onRemove={handleStatementRemoved}
                    files={statements}
                    message={
                      <span style={{ fontSize: '13px', color: 'black' }}>
                        <strong style={{ fontWeight: 600 }}>Choose a file </strong>or Drag it here
                    </span>
                    }
                    accept={'application/pdf'}
                  />
                </div>
                <span style={{ fontSize: '10px' }}>(* Only PDF statements downloaded from bank website, not the scanned ones)</span>

                <TextField
                  name="password"
                  fullWidth
                  hintText="Password"
                  floatingLabelText="Password (Optional)"
                  type="password"
                  minLen={6}
                  maxLen={15}
                  optional
                />
                <span style={{ fontSize: '10px' }}>(* If your documents are password protected, please enter the password.)</span>

                <div className={errorMsg ? '' : 'hidden'} style={{ ...styles.errorMsgStyle }}>
                  {errorMsg}
                </div>

              </div>)
          }


        </div>
      </LoanStep>
    );
  }
}

BankingPage.propTypes = {
  statementRecieved: PropTypes.func.isRequired,
  statementRemoved: PropTypes.func.isRequired,
  selectTab: PropTypes.func.isRequired,
  BankingPageState: PropTypes.object,
  bankNameChng: PropTypes.func.isRequired,
  setLoader: PropTypes.func.isRequired,
  updateStatusCalled: PropTypes.func.isRequired,
  setNetBankingLoader: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  BankingPageState: makeSelectBankingPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    statementRecieved: ({ file, dataUrl, type }) => dispatch(statementRecievedActionCreator({ file, dataUrl, type })),
    statementRemoved: (index) => dispatch(statementRemovedActionCreator({ index })),
    selectTab: (id) => dispatch({
      type: 'tab-selected',
      id,
    }),
    bankNameChng: (text) => dispatch(bankNameChangeActionCreator(text)),
    setLoader: (value) => dispatch({
      type: 'show-loader',
      value,
    }),
    updateStatusCalled: (value) => dispatch(updateStatusCalledFirstTimeActionCreator(value)),
    setNetBankingLoader: (value) => dispatch(netBankingLoaderActionCreator(value)),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BankingPage);
