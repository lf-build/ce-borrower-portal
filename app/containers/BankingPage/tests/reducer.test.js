
import { fromJS } from 'immutable';
import bankingPageReducer from '../reducer';

describe('bankingPageReducer', () => {
  it('returns the initial state', () => {
    expect(bankingPageReducer(undefined, {})).toEqual(fromJS({
      statements: [],
      errorMsg: undefined,
      isNetlinkingDone: false,
    }));
  });
});
