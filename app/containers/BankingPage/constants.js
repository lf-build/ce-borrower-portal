/*
 *
 * BankingPage constants
 *
 */

export const DEFAULT_ACTION = 'app/BankingPage/DEFAULT_ACTION';
export const STATEMENT_RECIEVED = 'app/BankingPage/STATEMENT_RECIEVED';
export const STATEMENT_REMOVED = 'app/BankingPage/STATEMENT_REMOVED';
export const BANK_NAME_CHANGE = 'app/BankingPage/BANK_NAME_CHANGE';
export const ERROR_OCCURRED = 'app/BankingPage/ERROR_OCCURRED';
export const SET_NET_LINK_TRANSACTION = 'app/BankingPage/SET_NET_LINK_TRANSACTION';
export const NET_LINK_TRANSACTION_FAILED = 'app/BankingPage/NET_LINK_TRANSACTION_FAILED';

