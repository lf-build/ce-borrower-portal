/*
 * BankingPage Messages
 *
 * This contains all the text for the BankingPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.BankingPage.header',
    defaultMessage: 'Please complete the verification process to finalise your interest rate',
  },
  apiErrorHeader: {
    id: 'app.containers.BankingPage.apiErrorHeader',
    defaultMessage: 'Oops! server is not responding. Please try again later after some time. Apologies for the inconvenience.',
  },
  statementErrorHeader: {
    id: 'app.containers.BankingPage.statementErrorHeader',
    defaultMessage: 'We cannot accept this bank statement as it seems to have been altered. Please ensure your bank statement(s) is / are in bank-generated, PDF format (pictures, scans and edited files can NOT be processed). You may upload multiple files. Do not tamper with the file in any way, including converting files or merging them.',
  },
});
