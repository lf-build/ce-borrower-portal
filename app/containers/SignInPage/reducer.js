/*
 *
 * SignInPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  PROMPT_FOR_REAPPLY,
  REAPPLY_ACCEPT,
  REAPPLY_DENIED,
  SEND_LOGIN_REQUEST_STARTED,
  SEND_LOGIN_REQUEST_ENDED,
  USERNAME_NOT_FOUND,
} from './constants';

const initialState = fromJS({
  promptForReapply: false,
});

function signInPageReducer(state = initialState, action) {
  switch (action.type) {
    case PROMPT_FOR_REAPPLY:
      return state.set('promptForReapply', true)
                  .set('isFreshApplication', action.payload.isFreshApplication);
    case REAPPLY_ACCEPT:
    case REAPPLY_DENIED:
      return state.set('promptForReapply', false);
    case SEND_LOGIN_REQUEST_STARTED:
      return state.set('showSendOtpLoader', true);
    case SEND_LOGIN_REQUEST_ENDED:
      return state.set('showSendOtpLoader', false);
    case USERNAME_NOT_FOUND:
      return state.set('notFound', action.notFound);
    default:
      return state;
  }
}

export default signInPageReducer;
