import { takeEvery, put, select } from 'redux-saga/effects';
// import { browserHistory } from 'react-router';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { SAVE_LOANSTEP_FULFILLED } from '../../containers/LoanStep/constants';
import { userSignedIn, loadUserProfile } from '../../sagas/auth/actions';
import { USER_SIGNED_IN } from '../../sagas/auth/constants';
import * as actions from './actions';
import { REAPPLY_ACCEPT, REAPPLY_DENIED, SEND_LOGIN_OTP } from './constants';

function* authenticationSaga(action) {
  yield put(userSignedIn(action.meta.response.meta.body.token, action.meta.response.meta.body.username));

  // Fullstory - updating the displayName, using which it will be easy to search user session.
  if (window.FS) {
    const currentSession = window.FS.getCurrentSession();
    const mobileNumber = action.meta.response.meta.body.username;
    window.FS.identify(currentSession, {
      displayName: mobileNumber,
    });
    window.FS.setUserVars({
      mobileNumber_str: mobileNumber,
    });
  }
}

function* askForReaply() {
  const { username } = (yield select((state) => state.get('auth'))).toJS();
  // const username = JSON.parse(atob(token.split('.')[1])).sub;
  // const { 'sign-in': { values: { username } } } = (yield select((state) => state.get('form'))).toJS();
  const tag = 'can-reapply';
  // Select username from store
  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'sign-up',
    command: 'eligible-for-reapply',
  }, {
    tag,
    payload: {
      attribute: 'mobile',
      value: username,
    },
  });

  if (executionRequestAccepted) {
    // Wait for workflow to either fail or succeed.
    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(tag);
    if (valid) {
      if (!valid.meta.body.eligible && !valid.meta.body.freshApplication) {
        yield put(loadUserProfile({
          flow: 'RETURN-USER',
          mobileNumber: username,
        }));
      } else {
        yield put(actions.promptForReapply(valid.meta.body.freshApplication));
      }
    }
  } else {
    // yield put(actions.generateInitialOfferDenied({ message: 'Workflow did not accept the request.' }));
    // navigationHelper.goTo({ name: 'initialOffer' }, 'internalServerError', false);
  }
}

function* enableReapplyInCoolOff(token) {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'sign-up',
    command: 'enable-reapply',
  }, {
    tag: 'enable-reapply-in-cool-off',
    payload: {
      mobileNumber: JSON.parse(atob(token.split('.')[1])).sub,
    },
  }))) {
    return;
  }

  yield put(actions.enableReapplyRequestStarted());

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('enable-reapply-in-cool-off');

  if (valid) {
    yield put(actions.enableReapplyRequestFulfilled(valid.meta.body));
  } else {
    yield put(actions.enableReapplyRequestFailed('error while enabling reapply in cool off period'));
  }

  yield put(actions.enableReapplyRequestEnded());
}

function* restoreProfile(action) {
  const { token, username } = (yield select((state) => state.get('auth'))).toJS();
  const { isFreshApplication } = (yield select((state) => state.get('signInPage'))).toJS();

  if (isFreshApplication) {
    yield enableReapplyInCoolOff(token);
  }

  yield put(loadUserProfile({
    flow: action.type === REAPPLY_ACCEPT ? 'RE-APPLY' : 'RETURN-USER',
    mobileNumber: username,
  }));
  //  Show dialogbox.
}

function* sendLoginOtp() {
  const appState = yield select();
  const forms = appState.toJS().form;
  const { 'sign-in': signIn } = forms;

  if (signIn) {
    const { values } = signIn;

    if (!values || !values.username) {
      yield put({
        type: '@@redux-form/FOCUS',
        meta: {
          form: 'sign-in',
          field: 'username',
        },
      });

      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'sign-in',
          field: 'username',
          touch: true,
        },
        payload: '',
      });

      yield put({
        type: '@@redux-form/UPDATE_SYNC_ERRORS',
        meta: {
          form: 'sign-in',
        },
        payload: {
          syncErrors: {
            username: 'Please enter Mobile Number to Send OTP',
            otp: !values.otp && 'This field is required',
          },
        },
      });
    } else {
      const { username } = values;

      if (!(yield uplink.requestUplinkExecution({
        dock: 'on-boarding',
        section: 'sign-up',
        command: 'send-login-otp',
      }, {
        tag: 'send-login-otp',
        payload: {
          username,
        },
      }))) {
        return;
      }

      yield put(actions.sendLoginOtpRequestStarted());

      const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('send-login-otp');

      if (valid) {
        yield put(actions.sendLoginOtpRequestFulfilled(valid.meta.body));
      } else {
        const {
          meta: { error: { body: { message: { body: { code } } } } },
        } = invalid;

        if (code === 400) {
          yield put(actions.usernameNotFound(true));
        }
        yield put(actions.sendLoginOtpRequestFailed('error while sending OTP'));
      }

      yield put(actions.sendLoginOtpRequestEnded());
    }
  }
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(`${SAVE_LOANSTEP_FULFILLED}_sign-in`, authenticationSaga);
  yield takeEvery(USER_SIGNED_IN, askForReaply);
  yield takeEvery(REAPPLY_ACCEPT, restoreProfile);
  yield takeEvery(REAPPLY_DENIED, restoreProfile);
  yield takeEvery(SEND_LOGIN_OTP, sendLoginOtp);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
