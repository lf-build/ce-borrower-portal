/*
 *
 * SignInPage actions
 *
 */

import {
  DEFAULT_ACTION,
  REAPPLY_ACCEPT,
  REAPPLY_DENIED,
  PROMPT_FOR_REAPPLY,
  ENABLE_REAPPLY_REQUEST,
  ENABLE_REAPPLY_REQUEST_STARTED,
  ENABLE_REAPPLY_REQUEST_FULFILLED,
  ENABLE_REAPPLY_REQUEST_FAILED,
  ENABLE_REAPPLY_REQUEST_ENDED,
  SEND_LOGIN_OTP,
  SEND_LOGIN_REQUEST,
  SEND_LOGIN_REQUEST_STARTED,
  SEND_LOGIN_REQUEST_FULFILLED,
  SEND_LOGIN_REQUEST_FAILED,
  SEND_LOGIN_REQUEST_ENDED,
  USERNAME_NOT_FOUND,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function acceptReapply() {
  return {
    type: REAPPLY_ACCEPT,
  };
}

export function denyReapply() {
  return {
    type: REAPPLY_DENIED,
  };
}

export function promptForReapply(isFreshApplication) {
  return {
    type: PROMPT_FOR_REAPPLY,
    payload: {
      isFreshApplication,
    },
  };
}

export function enableReapplyRequest() {
  return {
    type: ENABLE_REAPPLY_REQUEST,
  };
}

export function enableReapplyRequestStarted() {
  return {
    type: ENABLE_REAPPLY_REQUEST_STARTED,
  };
}

export function enableReapplyRequestFulfilled(form) {
  return {
    type: ENABLE_REAPPLY_REQUEST_FULFILLED,
    meta: {
      form,
    },
  };
}

export function enableReapplyRequestFailed(error) {
  return {
    type: ENABLE_REAPPLY_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function enableReapplyRequestEnded() {
  return {
    type: ENABLE_REAPPLY_REQUEST_ENDED,
  };
}

export function sendLoginOtp() {
  return {
    type: SEND_LOGIN_OTP,
  };
}

export function sendLoginOtpRequest() {
  return {
    type: SEND_LOGIN_REQUEST,
  };
}

export function sendLoginOtpRequestStarted() {
  return {
    type: SEND_LOGIN_REQUEST_STARTED,
  };
}

export function sendLoginOtpRequestFulfilled(form) {
  return {
    type: SEND_LOGIN_REQUEST_FULFILLED,
    meta: {
      form,
    },
  };
}

export function sendLoginOtpRequestFailed(error) {
  return {
    type: SEND_LOGIN_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function sendLoginOtpRequestEnded() {
  return {
    type: SEND_LOGIN_REQUEST_ENDED,
  };
}

export function usernameNotFound(notFound) {
  return {
    type: USERNAME_NOT_FOUND,
    notFound,
  };
}
