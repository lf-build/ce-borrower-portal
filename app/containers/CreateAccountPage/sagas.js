// import { takeEvery, put } from 'redux-saga/effects';
// import { SAVE_LOANSTEP_FULFILLED } from '../../containers/LoanStep/constants';
// import { userSignedIn, loadUserProfile } from '../../sagas/auth/actions';

// function* loginSaga(action) {
//   yield put(userSignedIn(action.meta.response.meta.body.token));
//   yield put(loadUserProfile({
//     flow: 'RETURN-USER',
//     token: action.meta.response.meta.body.token,
//   }));
// }
// import { takeEvery, put, select } from 'redux-saga/effects';
// import { SAVE_LOANSTEP_FULFILLED } from '../../containers/LoanStep/constants';
// import { userSignedIn, loadUserProfile } from '../../sagas/auth/actions';

// function* loginSaga(action) {
//   const { mobile: { values: { mobileNumber } } } = (yield select((state) => state.get('form'))).toJS();
//   yield put(userSignedIn(action.meta.response.meta.body.token));
//   yield put(loadUserProfile({
//     flow: 'RETURN-USER',
//     token: mobileNumber,
//   }));
// }

// Individual exports for testing
export function* defaultSaga() {
  // yield takeEvery(`${SAVE_LOANSTEP_FULFILLED}_otp`, loginSaga);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
