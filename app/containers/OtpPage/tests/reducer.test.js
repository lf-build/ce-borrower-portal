
import { fromJS } from 'immutable';
import otpPageReducer from '../reducer';

describe('otpPageReducer', () => {
  it('returns the initial state', () => {
    expect(otpPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
