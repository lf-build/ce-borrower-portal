/*
 * OtpPage Messages
 *
 * This contains all the text for the OtpPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.OtpPage.header',
    defaultMessage: 'Mobile number verification',
  },
});
