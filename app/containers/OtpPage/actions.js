/*
 *
 * OtpPage actions
 *
 */

import {
  DEFAULT_ACTION,
  RESEND_OTP_REQUEST,
  RESEND_OTP_REQUEST_STARTED,
  RESEND_OTP_REQUEST_FULFILLED,
  RESEND_OTP_REQUEST_FAILED,
  RESEND_OTP_REQUEST_ENDED,
  RESEND_VOICE_OTP_REQUEST,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function resendOtpRequest() {
  return {
    type: RESEND_OTP_REQUEST,
  };
}

export function resendVoiceOtpRequest() {
  return {
    type: RESEND_VOICE_OTP_REQUEST,
  };
}

export function resendOtpRequestStarted() {
  return {
    type: RESEND_OTP_REQUEST_STARTED,
  };
}

export function resendOtpRequestFulfilled(body) {
  return {
    type: RESEND_OTP_REQUEST_FULFILLED,
    meta: {
      body,
    },
  };
}

export function resendOtpRequestFailed(error) {
  return {
    type: RESEND_OTP_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function resendOtpRequestEnded() {
  return {
    type: RESEND_OTP_REQUEST_ENDED,
  };
}
