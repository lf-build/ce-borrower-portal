/*
 *
 * OtpPage
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';
import { getFormValues } from 'redux-form/immutable';
import { browserHistory } from 'react-router';
import InfoIcon from 'material-ui/svg-icons/alert/error';
import ReactTooltip from 'react-tooltip';
import { createStructuredSelector } from 'reselect';

import makeSelectOtpPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import { MobileIcon } from '../../components/Icons';
import { resendVoiceOtpRequest, resendOtpRequest as resendOtpRequestActionCreator } from './actions';
import makeSelectWelcomePage from '../WelcomePage/selectors';

const I = styled(InfoIcon)`
  color: #79cdd5 !important;
  transform: rotate(180deg) !important;
`;
const Link = styled.span`
  color: #79cdd5;
  cursor: pointer;
  text-align: center;
  z-index: 999;
  margin-top: 100;
`;
const ResendOtp = styled.span`
  float: right;
  color: #9da0a0;
  font-size: 14px;
  font-weight: 500;
  text-align: right;
  margin-bottom: 0;
  padding-top: 15px;
`;
const ResendVoiceOTP = styled.span``;
const selector = getFormValues('mobile');
const MobileFormSelector = (state) => selector(state); // && selector(state).toJS();

export class OtpPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    const { MobileForm } = this.props;
    const { mobileNumber } = MobileForm.toJS();

    // Fullstory - updating the displayName, using which it will be easy to search user session.
    if (window.FS) {
      const currentSession = window.FS.getCurrentSession();
      window.FS.identify(currentSession, {
        displayName: mobileNumber,
      });
      window.FS.setUserVars({
        mobileNumber_str: mobileNumber,
      });
    }
  }
  render() {
    const { MobileForm, resendOTP, resendVoiceOTP } = this.props;
    const { WelcomePage: { utmSource, utmMedium, utmCampaign, utmTerm, utmContent, gclid } } = this.props;

    if (!MobileForm) {
      return <span />;
    }
    const { mobileNumber } = MobileForm.toJS();
    const ShortDescription = () => (<p> A One Time Password has been sent to your mobile number +91 {mobileNumber.toString().replace('+91', '').replace(/ /g, '')} <Link onClick={() => browserHistory.replace('/application/mobile')}> Change </Link> </p>);
    ShortDescription.propTypes = {
      navigationHelper: PropTypes.object,
      MobileForm: PropTypes.object,
    };
    // const unmaskOtp = ({ otp }) => otp ? { otp: otp.replace(/ /g, '') } : {};

    return (
      <LoanStep
        noBack
        saveErrorClass={'otp'}
        payloadGenerator={() => ({
          utmSource: utmSource === null || utmSource === undefined || utmSource === 'undefined' ? undefined : utmSource,
          utmMedium: utmMedium === null || utmMedium === undefined || utmMedium === 'undefined' ? undefined : utmMedium,
          utmCampaign: utmCampaign === null || utmCampaign === undefined || utmCampaign === 'undefined' ? undefined : utmCampaign,
          utmTerm: utmTerm === null || utmTerm === undefined || utmTerm === 'undefined' ? undefined : utmTerm,
          utmContent: utmContent === null || utmContent === undefined || utmContent === 'undefined' ? undefined : utmContent,
          gclId: gclid === null || gclid === undefined || gclid === 'undefined' ? undefined : gclid,
        })}
        name={'otp'}
        title={messages.header}
        ShortDescription={ShortDescription}
      >
        <MobileIcon />
        <TextField
          enforceMasking
          name="otp"
          fullWidth
          hintText="* * * *"
          floatingLabelText="OTP"
          type="password"
          minLen={4}
          maxLen={4}
          mask={[/\d/, /\d/, /\d/, /\d/]}
        />
        <ResendOtp>Didn’t receive the OTP? <I style={{ cursor: 'pointer' }} data-tip="Resend OTP is allowed 3 times." data-place="bottom" data-type="light" data-effect="solid" data-border /><ReactTooltip />
          <br />
          <Link onClick={() => resendOTP()}>Resend</Link>
          &nbsp; | &nbsp;
        <ResendVoiceOTP><Link onClick={() => resendVoiceOTP()}>Resend OTP on call</Link></ResendVoiceOTP>
        </ResendOtp>
      </LoanStep>
    );
  }
}

OtpPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
  MobileForm: PropTypes.object,
  resendOTP: PropTypes.func,
  resendVoiceOTP: PropTypes.func,
  WelcomePage: PropTypes.object,
};

const mapStateToProps = createStructuredSelector({
  OtpPage: makeSelectOtpPage(),
  MobileForm: MobileFormSelector,
  WelcomePage: makeSelectWelcomePage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
    resendOTP: () => dispatch(resendOtpRequestActionCreator()),
    resendVoiceOTP: () => dispatch(resendVoiceOtpRequest()),
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(OtpPage);
