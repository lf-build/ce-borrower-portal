/*
 *
 * NetLinkEmbeddedForm reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
  // UPDATE_STATUS_CALLED_FIRST_TIME,
} from './constants';

const initialState = fromJS({
  // statusCalledFirstTime: true,
});

function netLinkEmbeddedFormReducer(state = initialState, action) {
  switch (action.type) {
    case DEFAULT_ACTION:
      return state;
    // case UPDATE_STATUS_CALLED_FIRST_TIME:
    //   console.log('UPDATE_STATUS_CALLED_FIRST_TIME called ', action.payload.status);
    //   state.set('statusCalledFirstTime', action.payload.status);
    //   console.log(state.toJS());
    //   return state;
    default:
      return state;
  }
}

export default netLinkEmbeddedFormReducer;
