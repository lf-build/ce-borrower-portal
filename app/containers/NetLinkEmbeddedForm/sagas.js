import { takeEvery, put, select } from 'redux-saga/effects';
import { browserHistory } from 'react-router';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import {
  GET_NET_LINK_STATUS_REQUEST,
  GET_NET_LINK_STATUS_REQUEST_FULFILLED,
  GET_NET_LINK_STATUS_REQUEST_FAILED,
  // NET_LINK_FORM_REQUEST,
} from './constants';
import * as netlinkActions from './actions';
import {
  SET_NET_LINK_TRANSACTION,
  NET_LINK_TRANSACTION_FAILED,
} from '../BankingPage/constants';
import { navigationHelper } from '../NavigationHelper/helper';

function* getNetlinkStatus() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'verify',
    command: 'get-netlink-status',
  }, {
    tag: 'netlink-status',
  }))) {
    return;
  }

  yield put(netlinkActions.netlinkStatusRequestStarted());

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('netlink-status');

  if (valid) {
    yield put(netlinkActions.netlinkStatusRequestFulfilled(valid.meta.body));
  } else {
    // checking if Netlink Status is called for the first time.
    const allStates = yield select();
    const stateObj = allStates.toJS();

    const { app: { statusCalledFirstTime } } = stateObj;
    if (!statusCalledFirstTime || statusCalledFirstTime) {
      yield getNetlinkForm();
    } else {
      yield put(netlinkActions.netlinkStatusRequestFailed('error while fetching Netlink Status'));
    }
  }

  yield put(netlinkActions.netlinkStatusRequestEnded());
}

function* netlinkFulfilled(action) {
  const { meta: { form: { body } } } = action;

  if (body === 'success') {
    yield put({
      type: SET_NET_LINK_TRANSACTION,
      meta: {
        form: 'banking',
      },
      payload: {
        status: true,
      },
    });

    browserHistory.replace('/application/social');
    // setTimeout(() => navigationHelper.goTo({ name: 'welcomePage' }, 'educationPage'), 1000);
  } else {
    // checking if Netlink Status is called for the first time.
    const allStates = yield select();
    const stateObj = allStates.toJS();

    const { app: { statusCalledFirstTime } } = stateObj;

    if (statusCalledFirstTime) {
      yield getNetlinkForm();
    } else {
      browserHistory.replace('/user/welcome/direct');
      // here we update the statusCalledFirstTime parameter to true so that the NetBanking form can be shown once again
      // it have got failed due to some reason i.e. some error from Perfios NetBanking connect or user have cancelled the
      // operation.
      yield put(netlinkActions.updateStatusCalledFirstTime(true));
      yield put({
        type: 'show-loader',
        value: false,
      });
      // setTimeout(() => navigationHelper.goTo({ name: 'welcomePage' }, 'bankingPage'), 1000);
      browserHistory.replace('/application/banking');
    }
  }
}

function* netlinkFailed(action) {
  const { meta: { error } } = action;

  if (error) {
    yield put({
      type: NET_LINK_TRANSACTION_FAILED,
      meta: {
        form: 'banking',
      },
      payload: {
        value: false,
      },
    });
    // here we update the statusCalledFirstTime parameter to true so that the NetBanking form can be shown once again
    // it have got failed due to some reason i.e. some error from Perfios NetBanking connect or user have cancelled the
    // operation.
    yield put(netlinkActions.updateStatusCalledFirstTime(true));
    setTimeout(() => navigationHelper.goTo({ name: 'netlink' }, 'bankingPage'), 1000);
  }
}

function* getNetlinkForm() {
  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'verify',
    command: 'get-netlink-form',
  }, {
    tag: 'netlink-form',
  }))) {
    return;
  }

  yield put(netlinkActions.netlinkFormRequestStarted());

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure('netlink-form');

  if (valid) {
    yield put(netlinkActions.netlinkFormRequestFulfilled(valid.meta.body.body));
  } else {
    yield put(netlinkActions.netlinkFormRequestFailed('error while fetching Netlink form'));
  }

  yield put(netlinkActions.netlinkFormRequestEnded());

  if (valid) {
    // here we update the statusCalledFirstTime parameter to false so that it will help in checking if NetBanking form
    // need to be shown again or not.
    yield put(netlinkActions.updateStatusCalledFirstTime(false));

    // here html form will come in body
    document.getElementById('netLinkPlaceHolder').innerHTML = valid.meta.body.body;
    setTimeout(() => document.getElementById('perfiosnetlinkform').submit(), 4500);
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery(GET_NET_LINK_STATUS_REQUEST, getNetlinkStatus);
  yield takeEvery(GET_NET_LINK_STATUS_REQUEST_FULFILLED, netlinkFulfilled);
  yield takeEvery(GET_NET_LINK_STATUS_REQUEST_FAILED, netlinkFailed);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
