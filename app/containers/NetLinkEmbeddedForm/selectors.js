import { createSelector } from 'reselect';

/**
 * Direct selector to the netLinkEmbeddedForm state domain
 */
const selectNetLinkEmbeddedFormDomain = () => (state) => state.get('netLinkEmbeddedForm');

/**
 * Other specific selectors
 */


/**
 * Default selector used by NetLinkEmbeddedForm
 */

const makeSelectNetLinkEmbeddedForm = () => createSelector(
  selectNetLinkEmbeddedFormDomain(),
  (substate) => substate.toJS()
);

export default makeSelectNetLinkEmbeddedForm;
export {
  selectNetLinkEmbeddedFormDomain,
};
