/*
 *
 * VerifyEmailPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import RaisedButton from 'material-ui/RaisedButton';
import { createStructuredSelector } from 'reselect';
import styled from 'styled-components';

import makeSelectVerifyEmailPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';

const VerifyEmailBlock = styled.div`
  color: #4e4e4e;
  font-size: 14px;
  padding: 10px;
  text-align: center;
`;

export class VerifyEmailPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep
        saveErrorClass={'verify-email'}
        name={'verify-email'}
        title={messages.header}
        reduceWidth
        navigationPanel={(props) => <RaisedButton disabled={props.isLoadingOrSubmiting} onTouchTap={props.handleGoNext} primary label="Verify" />}
      >
        <VerifyEmailBlock>
          Please click on below button to verify your email.
        </VerifyEmailBlock>
      </LoanStep>
    );
  }
}

VerifyEmailPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  VerifyEmailPage: makeSelectVerifyEmailPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VerifyEmailPage);
