
import { fromJS } from 'immutable';
import verifyEmailPageReducer from '../reducer';

describe('verifyEmailPageReducer', () => {
  it('returns the initial state', () => {
    expect(verifyEmailPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
