import { createSelector } from 'reselect';

/**
 * Direct selector to the verifyEmailPage state domain
 */
const selectVerifyEmailPageDomain = () => (state) => state.get('verifyEmailPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by VerifyEmailPage
 */

const makeSelectVerifyEmailPage = () => createSelector(
  selectVerifyEmailPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectVerifyEmailPage;
export {
  selectVerifyEmailPageDomain,
};
