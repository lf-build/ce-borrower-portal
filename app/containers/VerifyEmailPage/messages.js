/*
 * VerifyEmailPage Messages
 *
 * This contains all the text for the VerifyEmailPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.VerifyEmailPage.header',
    defaultMessage: 'Verify Email',
  },
});
