/*
 *
 * VerificationFailed
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import styled from 'styled-components';

import { createStructuredSelector } from 'reselect';
import makeSelectVerificationFailed from './selectors';
import messages from './messages';
import LoanStep from '../LoanStep';
import { DangerIcon } from '../../components/Icons';

const CongratulationText = styled.p`
    // font-size: 18px;
    // font-weight: 600;
    margin: 0 0 20px;
    text-align: center;
    font-size: 19px;
    line-height: 27px;
    font-weight: 100;
    letter-spacing: 0.025em;
`;

export class VerificationFailed extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    return (
      <LoanStep saveErrorClass={'otp'} navigationPanel={() => <span />} title={messages.header}>
        <DangerIcon />
        <CongratulationText />
        <CongratulationText>
          Oops! Verification was unsuccessful.
        </CongratulationText>
      </LoanStep>
    );
  }
}

VerificationFailed.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  VerificationFailed: makeSelectVerificationFailed(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(VerificationFailed);
