/*
 * VerificationFailed Messages
 *
 * This contains all the text for the VerificationFailed component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.VerificationFailed.header',
    defaultMessage: 'Verification Failed!',
  },
});
