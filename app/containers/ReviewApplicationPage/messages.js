/*
 * ReviewApplicationPage Messages
 *
 * This contains all the text for the ReviewApplicationPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ReviewApplicationPage.header',
    defaultMessage: 'Kindly review your loan application',
  },
  loadingHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingHeader',
    defaultMessage: 'Determining your credit eligibility',
  },
  loadingSubHeader: {
    id: 'app.containers.ReviewApplicationPage.loadingSubHeader',
    defaultMessage: 'Please wait while we determine your credit eligibility!',
  },
});
