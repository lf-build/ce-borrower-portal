import { put, takeLatest, takeEvery, select } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { navigationHelper } from '../NavigationHelper/helper';

// import { LOCATION_CHANGE } from 'react-router-redux';
import * as actionTypes from './constants';
import * as actions from './actions';
import * as navigationActionTypes from '../NavigationHelper/constants';

function* validateNames(basicInformation) {
  const { middleName, lastName } = basicInformation;

  if (middleName) {
    yield put({
      type: '@@redux-form/BLUR',
      meta: {
        form: 'basic-information',
        field: 'middleName',
        touch: true,
      },
    });
  }

  if (lastName) {
    yield put({
      type: '@@redux-form/BLUR',
      meta: {
        form: 'basic-information',
        field: 'lastName',
        touch: true,
      },
    });
  }
}

function* validateAddress(details) {
  const { currentResidentialAddress, permanentResidentialAddress } = details;

  if (currentResidentialAddress) {
    if (currentResidentialAddress.addressLine1) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'currentResidentialAddress.addressLine1',
          touch: true,
        },
      });
    }

    if (currentResidentialAddress.addressLine2) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'currentResidentialAddress.addressLine2',
          touch: true,
        },
      });
    }

    if (currentResidentialAddress.locality) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'currentResidentialAddress.locality',
          touch: true,
        },
      });
    }
  }

  if (permanentResidentialAddress) {
    if (permanentResidentialAddress.addressLine1) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'permanentResidentialAddress.addressLine1',
          touch: true,
        },
      });
    }

    if (permanentResidentialAddress.addressLine2) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'permanentResidentialAddress.addressLine2',
          touch: true,
        },
      });
    }

    if (permanentResidentialAddress.locality) {
      yield put({
        type: '@@redux-form/BLUR',
        meta: {
          form: 'personal-details',
          field: 'permanentResidentialAddress.locality',
          touch: true,
        },
      });
    }
  }
}

export function* submitApplicationRequest(action) {
  let submitApplication = true;
  const appState = yield select();
  const reviewPage = appState.toJS().reviewApplicationPage;

  if (reviewPage) {
    const { validationErrors } = reviewPage;

    if (validationErrors) {
      const { basicInformation, details } = validationErrors;

      if (basicInformation) {
        submitApplication = false;
        yield validateNames(basicInformation);

        yield put(actions.showValidationErrors(true));

        yield put({
          type: '@@redux-form/UPDATE_SYNC_ERRORS',
          meta: {
            form: 'basic-information',
          },
          payload: {
            syncErrors: {
              basicInformation,
            },
          },
        });
      }

      if (details) {
        submitApplication = false;

        yield validateAddress(details);

        yield put({
          type: '@@redux-form/UPDATE_SYNC_ERRORS',
          meta: {
            form: 'personal-details',
          },
          payload: {
            syncErrors: {
              details,
            },
          },
        });
      }
    }
  }

  if (submitApplication) {
    // calling workflow to get application number so that it can be passed in ThreatMetrix session_id
    yield put(actions.getApplicationNumberRequest());

    const form = 'review';
    // Select username from store
    const executionRequestAccepted = yield uplink.requestUplinkExecution({
      dock: 'on-boarding',
      section: 'application',
      command: 'submit',
    }, { tag: form, payload: { ip: action.meta.ip } });

    if (executionRequestAccepted) {
      yield put(actions.submitApplicationStarted());

      // Wait for workflow to either fail or succeed.
      const { valid, invalid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);
      if (valid) {
        yield put(actions.submitApplicationFulfilled(valid.meta));
      } else {
        navigationHelper.goTo({ name: 'review' }, 'internalServerError', false);
        yield put(actions.submitApplicationFailed(invalid.meta));
      }
    } else {
      navigationHelper.goTo({ name: 'review' }, 'internalServerError', false);
      yield put(actions.submitApplicationDenied({ message: 'uplink did not accept the request.' }));
    }

    yield put(actions.submitApplicationEnded());
  }
}

export function* navigateToProperPage(action) {
  if (action.meta.body.type === 'lead') {
    yield navigationHelper.goTo({
      name: 'review',
      params: {
      },
    }, 'applicationSubmittedPage');
    return;
  }
  yield navigationHelper.goTo({
    name: 'review',
    params: {
    },
  }, 'initialOfferPage');
}

// export function* navigateToProperPage(action) {
//   if (action.meta.body.status && action.meta.body.status === 'rejected') {
//     yield navigationHelper.goTo({
//       name: 'review',
//       params: {
//       },
//     }, 'applicationRejectedPage');
//     return;
//   }
//   yield navigationHelper.goTo({
//     name: 'review',
//     params: {
//     },
//   }, 'initialOfferPage');
// }

function* mapApplicationStarted() {
  // Ack. the request.
  yield put({
    type: navigationActionTypes.NAVIGATE_NEXT_REQUEST_STARTED,
    meta: {},
  });
}
function* mapApplicationAborted() {
  yield put({
    type: navigationActionTypes.NAVIGATE_NEXT_REQUEST_ABORTED,
    meta: {
      failedToValidate: false,
      failedToSave: true,
    },
  });
}

function* mapApplicationEnded() {
  yield put({
    type: navigationActionTypes.NAVIGATE_NEXT_REQUEST_ENDED,
    meta: {},
  });
}

function* detectIP() {
  try {
    const { ip } = yield fetch('https://api.ipify.org?format=json').then((r) => r.json());
    yield put({
      type: actionTypes.IP_DETECTED_SUCCESS,
      ip,
    });
  } catch (e) { // eslint-disable-line no-empty

  }
}

export function* getApplicationNumber() {
  const form = 'get-application-number';

  const executionRequestAccepted = yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'application',
    command: 'get-application-number',
  }, { tag: form });

  if (executionRequestAccepted) {
    yield put(actions.getApplicationNumberRequestStarted());

    const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(form);

    if (valid) {
      yield put(actions.getApplicationNumberRequestFulfilled(valid.meta.body));
      const applicationNumber = valid.meta.body;
      if (applicationNumber) {
        const script = document.createElement('script');
        script.src = `https://h.online-metrix.net/fp/tags.js?org_id=1dgdm0rx&session_id=LApp${applicationNumber}`;
        script.async = true;
        document.body.appendChild(script);
      }
    } else {
      yield put(actions.getApplicationNumberRequestFailed('error while fetching application number'));
    }

    yield put(actions.getApplicationNumberRequestEnded());
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // Watches for LOAD_REPOS actions and calls getRepos when one comes in.
  // By using `takeLatest` only the result of the latest API call is applied.
  // It returns task descriptor (just like fork) so we can continue execution
  // const watcher =
  yield takeLatest(actionTypes.SUBMIT_APPLICATION_REQUEST, submitApplicationRequest);
  yield takeEvery(actionTypes.SUBMIT_APPLICATION_REQUEST_STARTED, mapApplicationStarted);
  yield takeEvery(actionTypes.SUBMIT_APPLICATION_REQUEST_FAILED, mapApplicationAborted);
  yield takeEvery(actionTypes.SUBMIT_APPLICATION_REQUEST_ENDED, mapApplicationEnded);
  yield takeLatest(actionTypes.SUBMIT_APPLICATION_REQUEST_FULFILLED, navigateToProperPage);
  yield takeLatest(actionTypes.DETECT_IP_ADDRESS, detectIP);
  yield takeLatest(actionTypes.GET_APPLICATION_NUMBER_REQUEST, getApplicationNumber);
  // yield takeLatest(GENERATE_INITIAL_OFFER_REQUEST_FULFILLED, navigateToProperPage);

  // Suspend execution until location changes
  // yield take(LOCATION_CHANGE);
  // yield cancel(watcher);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
