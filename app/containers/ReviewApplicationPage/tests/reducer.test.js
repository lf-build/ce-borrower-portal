
import { fromJS } from 'immutable';
import reviewApplicationPageReducer from '../reducer';

describe('reviewApplicationPageReducer', () => {
  it('returns the initial state', () => {
    expect(reviewApplicationPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
