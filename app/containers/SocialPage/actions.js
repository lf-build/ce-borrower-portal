/*
 *
 * SocialPage actions
 *
 */

import {
  DEFAULT_ACTION,
  LENDO_CALL_REQUEST,
  GET_INITIAL_OFFER_INFO_REQUEST,
  GET_INITIAL_OFFER_INFO_REQUEST_STARTED,
  GET_INITIAL_OFFER_INFO_REQUEST_FULFILLED,
  GET_INITIAL_OFFER_INFO_REQUEST_ENDED,
  GET_INITIAL_OFFER_INFO_REQUEST_FAILED,
} from './constants';

export function defaultAction() {
  return {
    type: DEFAULT_ACTION,
  };
}

export function lendoCallRequest() {
  return {
    type: LENDO_CALL_REQUEST,
  };
}

export function getInitialOfferInfoRequest() {
  return {
    type: GET_INITIAL_OFFER_INFO_REQUEST,
  };
}

export function getInitialOfferInfoRequestStarted() {
  return {
    type: GET_INITIAL_OFFER_INFO_REQUEST_STARTED,
  };
}

export function getInitialOfferInfoRequestFulfilled(offerInfo) {
  return {
    type: GET_INITIAL_OFFER_INFO_REQUEST_FULFILLED,
    meta: {
      offerInfo,
    },
  };
}

export function getInitialOfferInfoRequestFailed(error) {
  return {
    type: GET_INITIAL_OFFER_INFO_REQUEST_FAILED,
    meta: {
      error,
    },
  };
}

export function getInitialOfferInfoRequestEnded() {
  return {
    type: GET_INITIAL_OFFER_INFO_REQUEST_ENDED,
  };
}
