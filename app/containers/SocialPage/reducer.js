/*
 *
 * SocialPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  DEFAULT_ACTION,
} from './constants';

const initialState = fromJS({
  pending: true,
});

function socialPageReducer(state = initialState, action) {
  switch (action.type) {
    case 'app/uplink/EXECUTE_UPLINK_REQUEST_FULFILLED_lendo':
      return state.set('enableLendo', false).set('pending', false);
    case 'app/uplink/EXECUTE_UPLINK_REQUEST_FAILED_lendo':
      return state.set('enableLendo', true).set('pending', false);
    case DEFAULT_ACTION:
      return state;
    default:
      return state;
  }
}

export default socialPageReducer;
