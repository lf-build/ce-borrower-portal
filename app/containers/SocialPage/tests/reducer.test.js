
import { fromJS } from 'immutable';
import socialPageReducer from '../reducer';

describe('socialPageReducer', () => {
  it('returns the initial state', () => {
    expect(socialPageReducer(undefined, {})).toEqual(fromJS({ pending: true }));
  });
});
