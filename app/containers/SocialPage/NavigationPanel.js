/**
*
* SocialPageNavigationPanel
*
*/

import React from 'react';
// import styled from 'styled-components';
import RaisedButton from 'material-ui/RaisedButton';

export class SocialPageNavigationPanel extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const { isLoadingOrSubmiting, handleGoBack, handleGoNext, showSkip, buttonLabel } = this.props; // eslint-disable-line
    return (
      // <div className="col-xs-9 col-xs-offset-2">
      <div style={{ textAlign: 'center' }}>
        {/* <div className="col-xs-6">
          <RaisedButton
            style={{ width: '70%' }}
            disabled={isLoadingOrSubmiting}
            onTouchTap={handleGoBack}
            label="Back"
            labelColor="#3663ad"
            labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
            backgroundColor="#fff"
            buttonStyle={{ border: `${isLoadingOrSubmiting ? '' : '1px solid #3663ad'}` }}
          />
        </div> */}
        <div>
          <RaisedButton
            // style={{ width: '70%' }}
            disabled={isLoadingOrSubmiting || !showSkip}
            onTouchTap={handleGoNext}
            label={buttonLabel}
            labelColor="#fff"
            labelStyle={{ fontSize: '12px', letterSpacing: '1.2px' }}
            backgroundColor="#3663ad"
          />
        </div>
      </div>);
  }
}

SocialPageNavigationPanel.propTypes = {
  isLoadingOrSubmiting: React.PropTypes.bool.isRequired,
  handleGoBack: React.PropTypes.func.isRequired,
  handleGoNext: React.PropTypes.func.isRequired,
  showSkip: React.PropTypes.bool.isRequired,
  buttonLabel: React.PropTypes.string,
};

export default SocialPageNavigationPanel;
