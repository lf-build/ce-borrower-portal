/*
 * SocialPage Messages
 *
 * This contains all the text for the SocialPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.SocialPage.header',
    defaultMessage: 'Please complete the verification process to finalise your interest rate',
  },
});
