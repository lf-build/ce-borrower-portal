import { takeEvery, put, select } from 'redux-saga/effects';
function* resetRentOrEMI(action) {
  if (action.meta && action.meta.form === 'residence-expense-details') {
    if (action.meta.field === 'residenceType') {
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'hasHomeLoan',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });
      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'homeLoanEmi',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });

      yield put({
        type: '@@redux-form/CHANGE',
        meta: {
          form: 'residence-expense-details',
          field: 'monthlyRent',
          touch: false,
          persistentSubmitErrors: false,
        },
        payload: '',
      });
    }

    if (action.meta.field === 'hasHomeLoan') {
      // query the state using the exported selector
      const residenceExp = yield select();
      const { 'residence-expense-details': residenceExpState } = residenceExp.toJS().form;

      // when page is loaded for the first time
      if (!residenceExpState || !residenceExpState.values) {
        return;
      }

      const { values: { hasHomeLoan, homeLoanEmi } } = residenceExpState;
      if (hasHomeLoan && hasHomeLoan === 'false') {
        if (homeLoanEmi && (/[^\d]/.test(homeLoanEmi) || (homeLoanEmi.match(/^[0-9]+$/i) && homeLoanEmi.length > 6))) {
          yield put({
            type: '@@redux-form/CHANGE',
            meta: {
              form: 'residence-expense-details',
              field: 'homeLoanEmi',
              touch: false,
              persistentSubmitErrors: false,
            },
            payload: '',
          });
        }
      }
    }
  }
}
// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery('@@redux-form/CHANGE', resetRentOrEMI);
  // See example in containers/HomePage/sagas.js
}

// All sagas to be loaded
export default [
  defaultSaga,
];
