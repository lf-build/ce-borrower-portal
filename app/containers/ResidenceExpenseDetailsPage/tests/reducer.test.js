
import { fromJS } from 'immutable';
import residenceExpenseDetailsPageReducer from '../reducer';

describe('residenceExpenseDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(residenceExpenseDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
