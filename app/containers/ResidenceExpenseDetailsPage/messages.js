/*
 * ResidenceExpenseDetailsPage Messages
 *
 * This contains all the text for the ResidenceExpenseDetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.ResidenceExpenseDetailsPage.header',
    defaultMessage: 'A little about expenses',
  },
});
