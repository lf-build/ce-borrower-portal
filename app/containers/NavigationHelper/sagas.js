import { takeEvery, race, take, call, put, select } from 'redux-saga/effects';
import { submit, actionTypes, getFormValues } from 'redux-form/immutable';
import { navigationHelper } from './helper';

import {
  // DISABLE_ROUTE,
  NAVIGATE_NEXT_REQUEST,
  NAVIGATE_NEXT_REQUEST_STARTED,
  NAVIGATE_NEXT_REQUEST_FULFILLED,
  NAVIGATE_NEXT_REQUEST_ABORTED,
  NAVIGATE_NEXT_REQUEST_ENDED,
} from './constants';

import {
  SAVE_LOANSTEP_REQUEST,
  SAVE_LOANSTEP_FULFILLED,
  SAVE_LOANSTEP_FAILED,
  SAVE_LOANSTEP_EXECUTION_DENIED,
  LOAN_STEP_LOADED,
} from '../LoanStep/constants';
// Redux form submit success and failure action types.
const { SET_SUBMIT_SUCCEEDED, SET_SUBMIT_FAILED } = actionTypes;

function* save(action) {
  const { form } = action.meta;
  // Ack. the request.
  yield put({
    type: SAVE_LOANSTEP_REQUEST,
    meta: action.meta,
  });

  const { valid } = yield race({
    valid: take(`${SAVE_LOANSTEP_FULFILLED}_${form}`),
    invalid: take(`${SAVE_LOANSTEP_FAILED}_${form}`),
    denied: take(SAVE_LOANSTEP_EXECUTION_DENIED),
  });

  return valid;
}

function* navigateNext(action) {
  // Extract from route.
  // form is name of the form from where the next request started.
  // payload is extracted is actually extra information which is provided by
  // payloadGenerator prop to <StepForm .../> component.
  const { form, payloadGenerator } = action.meta.route;

  // Ack. the request.
  yield put({
    type: NAVIGATE_NEXT_REQUEST_STARTED,
    meta: action.meta,
  });

  // If form exsists,
  if (form) {
    // Trigger validation of the form.
    yield put(submit(form));
    const { valid } = yield race({
      valid: take(`${SET_SUBMIT_SUCCEEDED}_${form}`),
      invalid: take(`${SET_SUBMIT_FAILED}_${form}`),
    });
    // If its valid
    if (valid) {
      let { meta: { values } } = valid;
      values = { ...values || {}, ...payloadGenerator(values || {}) };
      const response = yield call(save, { meta: { values, form } });
      // No response === unable to save.
      if (!response) {
        // hence abort the navigation.
        yield put({ type: NAVIGATE_NEXT_REQUEST_ABORTED, meta: { failedToValidate: false, failedToSave: true, ...action.meta } });
      } else {
        // Go to next route.
        navigationHelper.goNext(action.meta.route);
        yield take(LOAN_STEP_LOADED);
        // Raise Navigation Done action.
        yield put({ type: NAVIGATE_NEXT_REQUEST_FULFILLED, meta: { response } });
      }
    } else {
      // Raise navigation Failed action.
      yield put({ type: NAVIGATE_NEXT_REQUEST_ABORTED, meta: { failedToValidate: true, failedToSave: false, ...action.meta } });
    }
  } else {
    navigationHelper.goNext(action.meta.route);
    // Suspend execution until location changes
    yield take(LOAN_STEP_LOADED);
    yield put({ type: NAVIGATE_NEXT_REQUEST_FULFILLED, meta: action.meta });
  }

  // Ack. that request ended. This does not describe sucess or failure.
  yield put({ type: NAVIGATE_NEXT_REQUEST_ENDED, meta: action.meta });
}

// Individual exports for testing
export function* defaultSaga() {
  yield takeEvery(NAVIGATE_NEXT_REQUEST, navigateNext);
}


function* mapReduxFormSubmitStatusSaga(action) {
  const state = yield select();
  let values = getFormValues(action.meta.form)(state) || {};
  if (values.toJS) {
    values = values.toJS();
  }

  yield put({ type: `${action.type}_${action.meta.form}`, meta: { values } });
}


export function* mapFormSucessSaga() {
  yield takeEvery(SET_SUBMIT_SUCCEEDED, mapReduxFormSubmitStatusSaga);
  yield takeEvery(SET_SUBMIT_FAILED, mapReduxFormSubmitStatusSaga);
}


// All sagas to be loaded
export default [
  defaultSaga,
  mapFormSucessSaga,
];
