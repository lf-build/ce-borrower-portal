
import {
  disableRoute,
} from '../actions';
import {
  DISABLE_ROUTE,
} from '../constants';

describe('NavigationHelper actions', () => {
  describe('Default Action', () => {
    it('has a type of DISABLE_ROUTE', () => {
      const route = 'SomeRoot';
      const expected = {
        type: DISABLE_ROUTE,
        route,
      };
      expect(disableRoute(route)).toEqual(expected);
    });
  });
});
