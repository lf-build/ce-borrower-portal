/*
 *
 * NavigationHelper constants
 *
 */

export const DISABLE_ROUTE = 'app/NavigationHelper/DISABLE_ROUTE';
export const NAVIGATE_NEXT_REQUEST = 'app/NavigationHelper/NAVIGATE_NEXT_REQUEST';
export const NAVIGATE_NEXT_REQUEST_STARTED = 'app/NavigationHelper/NAVIGATE_NEXT_REQUEST_STARTED';
export const NAVIGATE_NEXT_REQUEST_FULFILLED = 'app/NavigationHelper/NAVIGATE_NEXT_REQUEST_FULFILLED';
export const NAVIGATE_NEXT_REQUEST_ABORTED = 'app/NavigationHelper/NAVIGATE_NEXT_REQUEST_ABORTED';
export const NAVIGATE_NEXT_REQUEST_ENDED = 'app/NavigationHelper/NAVIGATE_NEXT_REQUEST_ENDED';
