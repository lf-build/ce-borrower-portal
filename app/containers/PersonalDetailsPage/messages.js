/*
 * PersonalDetailsPage Messages
 *
 * This contains all the text for the PersonalDetailsPage component.
 */
import { defineMessages } from 'react-intl';

export default defineMessages({
  header: {
    id: 'app.containers.PersonalDetailsPage.header',
    defaultMessage: 'A few final details',
  },
});
