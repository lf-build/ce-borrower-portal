import { createSelector } from 'reselect';

/**
 * Direct selector to the personalDetailsPage state domain
 */
const selectPersonalDetailsPageDomain = () => (state) => state.get('personalDetailsPage');

/**
 * Other specific selectors
 */


/**
 * Default selector used by PersonalDetailsPage
 */

const makeSelectPersonalDetailsPage = () => createSelector(
  selectPersonalDetailsPageDomain(),
  (substate) => substate.toJS()
);

export default makeSelectPersonalDetailsPage;
export {
  selectPersonalDetailsPageDomain,
};
