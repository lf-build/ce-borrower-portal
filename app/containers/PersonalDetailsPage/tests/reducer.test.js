
import { fromJS } from 'immutable';
import personalDetailsPageReducer from '../reducer';

describe('personalDetailsPageReducer', () => {
  it('returns the initial state', () => {
    expect(personalDetailsPageReducer(undefined, {})).toEqual(fromJS({}));
  });
});
