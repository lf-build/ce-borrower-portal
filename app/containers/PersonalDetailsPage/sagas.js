import { takeEvery, put, select } from 'redux-saga/effects';
import * as uplink from '@sigma-infosolutions/uplink/sagas/uplink/actions';
import { SET_DOB_ERROR_MSG, SET_YM_ERROR_MSG } from './constants';


function* fetchCityStateFromZip(action) {
  let section = '';
  if (action.meta.form === 'personal-details' && action.meta.field === 'currentResidentialAddress.pinCode') {
    section = 'currentResidentialAddress';
  } else if (action.meta.form === 'personal-details' && action.meta.field === 'permanentResidentialAddress.pinCode') {
    section = 'permanentResidentialAddress';
  } else {
    return;
  }

  if (!(yield uplink.requestUplinkExecution({
    dock: 'on-boarding',
    section: 'lookup',
    command: 'pincode-details',
  },
    {
      tag: action.payload,
      payload: {
        pincode: action.payload,
      },
    }))) {
    return;
  }

  const { valid } = yield uplink.waitForUplinkExecutionSuccessOrFailure(action.payload);
  if (valid) {
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'personal-details',
        field: `${section}.city`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: valid.meta.body.cities[0],
    });
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'personal-details',
        field: `${section}.state`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: valid.meta.body.state,
    });
  } else {
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'personal-details',
        field: `${section}.city`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: '',
    });
    yield put({
      type: '@@redux-form/CHANGE',
      meta: {
        form: 'personal-details',
        field: `${section}.state`,
        touch: false,
        persistentSubmitErrors: false,
      },
      payload: '',
    });

    if (section === 'currentResidentialAddress') {
      yield put({
        type: '@@redux-form/UPDATE_SYNC_ERRORS',
        meta: {
          form: 'personal-details',
        },
        payload: {
          syncErrors: {
            currentResidentialAddress: {
              pinCode: 'This Pin code is currently not serviceable.',
            },
          },
        },
      });
    }

    if (section === 'permanentResidentialAddress') {
      yield put({
        type: '@@redux-form/UPDATE_SYNC_ERRORS',
        meta: {
          form: 'personal-details',
        },
        payload: {
          syncErrors: {
            permanentResidentialAddress: {
              pinCode: 'This Pin code is currently not serviceable.',
            },
          },
        },
      });
    }
  }
}

function checkIfValidDOB(dob) {
  const errorMsg = dob.match(/^(?:(?:31(\/|-|\.)(?:0?[13578]|1[02]|(?:Jan|Mar|May|Jul|Aug|Oct|Dec)))\1|(?:(?:29|30)(\/|-|\.)(?:0?[1,3-9]|1[0-2]|(?:Jan|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec))\2))(?:(?:1[6-9]|[2-9]\d)?\d{2})$|^(?:29(\/|-|\.)(?:0?2|(?:Feb))\3(?:(?:(?:1[6-9]|[2-9]\d)?(?:0[48]|[2468][048]|[13579][26])|(?:(?:16|[2468][048]|[3579][26])00))))$|^(?:0?[1-9]|1\d|2[0-8])(\/|-|\.)(?:(?:0?[1-9]|(?:Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep))|(?:1[0-2]|(?:Oct|Nov|Dec)))\4(?:(?:1[6-9]|[2-9]\d)?\d{2})$/)
    ? undefined
    : 'Date Of Birth must be a valid date';

  if (errorMsg === undefined) {
    const initialDob = dob.split(/\//);
    const dateOfBirth = new Date([initialDob[1], initialDob[0], initialDob[2]].join('/'));
    const todaysDate = new Date();
    if (dateOfBirth.setHours(0, 0, 0, 0) >= todaysDate.setHours(0, 0, 0, 0)) {
      return "The date doesn't look right. Be sure to use your actual date of birth.";
    }
  }

  return errorMsg;
}
function checkIfValidYM(year) {
  const errorMsg = (year === '0')
    ? 'year can not be zero'
    : undefined;

  if (errorMsg !== undefined) {
    return errorMsg;
  }

  return errorMsg;
}

function* validateDOB(action) {
  const { meta: { form, field } } = action;

  if (form === 'personal-details' && (field === 'date' || field === 'month' || field === 'year')) {
    const persDetails = yield select();
    const { 'personal-details': personalDetails } = persDetails.toJS().form;
    const { values: { date, month, year } } = personalDetails;

    if (date && month && year) {
      const dob = `${date}/${month}/${year}`;

      const errorMsg = checkIfValidDOB(dob);
      if (errorMsg) {
        yield put({
          type: SET_DOB_ERROR_MSG,
          meta: {
            form: 'personal-details',
          },
          payload: {
            errorMsg,
          },
        });
      } else {
        yield put({
          type: SET_DOB_ERROR_MSG,
          meta: {
            form: 'personal-details',
          },
          payload: {
            errorMsg: undefined,
          },
        });
      }
    }
  }
}

function* validateYearMonth(action) {
  const { meta: { form, field } } = action;

  if (form === 'personal-details' && (field === 'currentAddressYear')) {
    const persDetails = yield select();
    const { 'personal-details': personalDetails } = persDetails.toJS().form;
    const { values: { currentAddressYear } } = personalDetails;
    if (currentAddressYear) {
      const errorMsg = checkIfValidYM(currentAddressYear);
      if (errorMsg) {
        yield put({
          type: SET_YM_ERROR_MSG,
          meta: {
            form: 'personal-details',
          },
          payload: {
            errorMsg,
          },
        });
      } else {
        yield put({
          type: SET_YM_ERROR_MSG,
          meta: {
            form: 'personal-details',
          },
          payload: {
            errorMsg: undefined,
          },
        });
      }
    }
  }
}

// Individual exports for testing
export function* defaultSaga() {
  // See example in containers/HomePage/sagas.js
  yield takeEvery('@@redux-form/BLUR', fetchCityStateFromZip);
  yield takeEvery('@@redux-form/CHANGE', validateDOB);
  yield takeEvery('@@redux-form/CHANGE', validateYearMonth);
}

// All sagas to be loaded
export default [
  defaultSaga,
];
