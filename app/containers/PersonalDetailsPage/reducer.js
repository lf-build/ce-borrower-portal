/*
 *
 * PersonalDetailsPage reducer
 *
 */

import { fromJS } from 'immutable';
import {
  // DEFAUL/T_ACTION,
} from './constants';

const initialState = fromJS({});

function personalDetailsPageReducer(state = initialState, action) {
  switch (action.type) {
    default:
      return state;
  }
}

export default personalDetailsPageReducer;
