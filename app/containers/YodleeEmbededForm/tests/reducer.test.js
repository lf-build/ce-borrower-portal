
import { fromJS } from 'immutable';
import yodleeEmbededFormReducer from '../reducer';

describe('yodleeEmbededFormReducer', () => {
  it('returns the initial state', () => {
    expect(yodleeEmbededFormReducer(undefined, {})).toEqual(fromJS({}));
  });
});
