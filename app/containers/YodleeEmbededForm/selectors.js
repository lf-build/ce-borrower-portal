import { createSelector } from 'reselect';

/**
 * Direct selector to the yodleeEmbededForm state domain
 */
const selectYodleeEmbededFormDomain = () => (state) => state.get('yodleeEmbededForm');

/**
 * Other specific selectors
 */


/**
 * Default selector used by YodleeEmbededForm
 */

const makeSelectYodleeEmbededForm = () => createSelector(
  selectYodleeEmbededFormDomain(),
  (substate) => (substate && substate.toJS()) || undefined
);

export default makeSelectYodleeEmbededForm;
export {
  selectYodleeEmbededFormDomain,
};
