/*
 *
 * YodleeEmbededForm
 *
 */

import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
// import { FormattedMessage } from 'react-intl';
import { createStructuredSelector } from 'reselect';
import makeSelectYodleeEmbededForm from './selectors';
// import messages from './messages';

export class YodleeEmbededForm extends React.Component { // eslint-disable-line react/prefer-stateless-function
  componentDidMount() {
    // document.getElementById('fastlinkForm').submit();
    const form = '<form id=\'fastlinkForm\' style=\'display:none\' action=\'https://yieapnode.stage.yodleeinteractive.com/authenticate/private-CreditExchange/?channelAppName=yieap\' method=\'POST\'><input type=\'text\' name=\'app\' value=\'10003600\' /><input type=\'text\' name=\'rsession\' value=\'07152016_1:d4c30120b4feb3204146579b5296c6d5359487adff46bc85cb1e2a251971416faae27adc3a1f054db7e3fa0237d79c2e921c7e5a8f2b00c14ba70392a486d855\' /><input type=\'text\' name=\'token\'  value=\'1553f02d37827a39788418bbf134e2bf919cf5f12c50b248f83f1e10ec487c9d\' /><input type=\'text\' name=\'redirectReq\' value=\'True\'/><input type=\'text\' name=\'extraParams\' value=\'\' /></form><script>document.getElementById(\'fastlinkForm\').submit();</script>';
    document.getElementById('yodleePlaceHolder').innerHTML = form;
    setTimeout(() => document.getElementById('fastlinkForm').submit());
  }

  render() {
    return (
      <div id="yodleePlaceHolder">
      </div>
    );
  }
}

YodleeEmbededForm.propTypes = {
  dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  YodleeEmbededForm: makeSelectYodleeEmbededForm(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(YodleeEmbededForm);
