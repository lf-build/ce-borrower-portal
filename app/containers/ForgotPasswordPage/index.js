/*
 *
 * ForgotPasswordPage
 *
 */

import React from 'react';
import { connect } from 'react-redux';
import { createStructuredSelector } from 'reselect';
import RaisedButton from 'material-ui/RaisedButton';

import makeSelectForgotPasswordPage from './selectors';
import messages from './messages';
import LoanStep from '../../components/LoanStepForm';
import TextField from '../../components/RequiredTextField';
import { MobileIcon } from '../../components/Icons';
import { mobileMask, normalizeMobile } from '../../components/MaskedTextField/masks';

const shortDescription = () => <p> Please enter your Mobile number. A reset password link will be sent to your Personal Email address. </p>;

export class ForgotPasswordPage extends React.Component { // eslint-disable-line react/prefer-stateless-function
  render() {
    const maskNumber = ({ username }) => username ? { username: normalizeMobile(username) } : {};

    return (
      <LoanStep
        payloadGenerator={maskNumber}
        saveErrorClass={'forgot-password'}
        name={'forgot-password'}
        title={messages.header}
        ShortDescription={shortDescription}
        reduceWidth
        navigationPanel={(props) =>
          <div style={{ textAlign: 'center' }}>
            <RaisedButton disabled={props.isLoadingOrSubmiting} onTouchTap={props.handleGoNext} primary label="Send" />
          </div>
        }
      >
        <MobileIcon />

        <div className="row">
          <div className={'mobile-label-div'}>
            <label
              htmlFor="username"
              className="prefix"
              style={{ paddingRight: '5px' }}
            >+91</label>
          </div>
          <div className={'mobile-textfield-div'}>
            <TextField
              name="username"
              fullWidth
              hintText=""
              floatingLabelText="Mobile Number"
              extraValidators={[(value) => value && normalizeMobile(value.toString()).match(/^[6-9][0-9]{9}$/) ? undefined : 'Mobile number is invalid']}
              mask={mobileMask}
            />
          </div>

        </div>

      </LoanStep>
    );
  }
}

ForgotPasswordPage.propTypes = {
  // dispatch: PropTypes.func.isRequired,
};

const mapStateToProps = createStructuredSelector({
  ForgotPasswordPage: makeSelectForgotPasswordPage(),
});

function mapDispatchToProps(dispatch) {
  return {
    dispatch,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordPage);
