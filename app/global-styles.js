import { injectGlobal } from 'styled-components';

/* eslint no-unused-expressions: 0 */
injectGlobal`
  html,
  body {
    height: 100%;
    width: 100%;
    // overflow-x: hidden;
    color: #4e4e4e;
  }

  body {
    font-family: open-sans, sans-serif;
  }

  body.fontLoaded {
    font-family: open-sans, sans-serif
  }

  #app, #app > div {
    background-color: #fff;
    min-height: 100%;
    // height: 100%;    
    min-width: 100%;
    width: 100%;
  }

  p,
  label {
    font-family: open-sans, sans-serif
    line-height: 1.5em;
  }

`;
